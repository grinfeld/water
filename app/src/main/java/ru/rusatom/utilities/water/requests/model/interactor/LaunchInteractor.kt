package ru.rusatom.utilities.water.requests.model.interactor

import ru.rusatom.utilities.water.requests.di.DI
import ru.rusatom.utilities.water.requests.di.module.ServerModule
import ru.rusatom.utilities.water.requests.model.data.storage.Prefs
import timber.log.Timber
import toothpick.Toothpick
import javax.inject.Inject

class LaunchInteractor @Inject constructor(
    private val prefs: Prefs
) {

    val hasAccount = prefs.accounts != null

    val isFirstLaunch: Boolean
        get() {
            val timeStamp = prefs.firstLaunchTimeStamp
            if (timeStamp == null) {
                prefs.firstLaunchTimeStamp = System.currentTimeMillis()
                return true
            } else {
                return false
            }
        }


    fun signInToLastSession() {
        if (!Toothpick.isScopeOpen(DI.SERVER_SCOPE)) {
            Timber.d("Init new scope: ${DI.SERVER_SCOPE} -> ${DI.APP_SCOPE}")
            Toothpick
                .openScopes(DI.APP_SCOPE, DI.SERVER_SCOPE)
                .installModules(ServerModule(prefs.serverPath))
        }
    }

}