package ru.rusatom.utilities.water.requests.entity

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import ru.rusatom.utilities.water.requests.R

enum class StatusOrderVehicleTS (
    val status: Int,
    @StringRes val strName: Int,
    @DrawableRes val drawableRes: Int
)  {

    APPOINTED(0, R.string.order_ts_status_appointed, R.drawable.bg_status_appointed),
    CONFIRMED(1, R.string.order_ts_status_confirmed, R.drawable.bg_status_confirmed),
    WAY(2, R.string.order_ts_status_way, R.drawable.bg_status_complete),
    WORK(3, R.string.order_ts_status_in_work, R.drawable.bg_status_in_work),
    COMPLETE(4, R.string.order_ts_status_complete, R.drawable.bg_status_complete);


}