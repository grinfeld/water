package ru.rusatom.utilities.water.requests.entity.status

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.status.StatusImpl.StatusValue.*

enum class StatusImpl(
    private val statusValues: ArrayList<StatusValue>
) : Status {

    ORDER_VEHICLE(arrayListOf(NEW, APPOINTED, CONFIRMED, WAY, WORK, COMPLETE, WAIT)),
    TASK(arrayListOf(NEW, APPOINTED, CONFIRMED, WORK, WAIT, COMPLETE, REJECTED)),
    ORDER_TMC(arrayListOf(NEW, PROCESSING, REJECTED, PROVIDED));

    override fun text(status: Int, context: Context): String =
        context.getString(statusValues[status].strName)

    override fun drawable(status: Int, context: Context): Drawable =
        ContextCompat.getDrawable(context, statusValues[status].drawableRes)!!

    override fun getLabelsStatus(context: Context): ArrayList<String> {
        val result = ArrayList<String>()
        for (i in 0 until statusValues.size) {
            result.add(text(i, context))
        }
        return result
    }

    private enum class StatusValue(
        @StringRes val strName: Int,
        @DrawableRes val drawableRes: Int
    ) {
        NEW(R.string.tasks_status_new, R.drawable.bg_status_new),
        APPOINTED(R.string.order_ts_status_appointed, R.drawable.bg_status_appointed),
        CONFIRMED(R.string.order_ts_status_confirmed, R.drawable.bg_status_confirmed),
        WAY(R.string.order_ts_status_way, R.drawable.bg_status_complete),
        WORK(R.string.order_ts_status_in_work, R.drawable.bg_status_in_work),
        COMPLETE(R.string.order_ts_status_complete, R.drawable.bg_status_complete),
        WAIT(R.string.tasks_status_wait, R.drawable.bg_status_wait),
        PROCESSING(R.string.tasks_order_ts_in_processing, R.drawable.bg_status_processing),
        REJECTED(R.string.tasks_order_ts_rejected, R.drawable.bg_status_rejected),
        PROVIDED(R.string.tasks_order_ts_provided, R.drawable.bg_status_provided)
    }
}