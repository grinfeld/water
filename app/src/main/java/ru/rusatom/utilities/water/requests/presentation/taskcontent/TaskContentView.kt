package ru.rusatom.utilities.water.requests.presentation.taskcontent

import com.arellomobile.mvp.MvpView

interface TaskContentView : MvpView {
    fun showProgress(b: Boolean)
    fun changeStatus(it: Int)
    fun showMessage(it: String)
}