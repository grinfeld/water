package ru.rusatom.utilities.water.requests.ui.notification

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_notification.recycler
import kotlinx.android.synthetic.main.fragment_notification.toolbar
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.Notification
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.utils.addSystemBottomPadding
import ru.rusatom.utilities.water.requests.utils.addSystemTopPadding

class NotificationFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_notification

    private lateinit var notificationAdapter: NotificationAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.apply {
            addSystemTopPadding()
        }
        view.addSystemBottomPadding()
        notificationAdapter = NotificationAdapter()
        recycler.layoutManager = LinearLayoutManager(requireContext())
        recycler.adapter = notificationAdapter
        val list = mutableListOf<Notification>()
        for (i in 1..5){
            list.add(Notification("Задание #$i", "01.02.2021 14:00"))
        }
        notificationAdapter.submitList(list)
    }

}