package ru.rusatom.utilities.water.requests.model.interactor

import io.reactivex.Single
import ru.rusatom.utilities.water.requests.model.server.Api
import ru.rusatom.utilities.water.requests.model.system.SchedulersProvider
import javax.inject.Inject

class OrderVehicleContentInteractor @Inject constructor(
    private val api: Api,
    private val schedulers: SchedulersProvider
) {
    fun changeStatus(orderId: Int, status: Int): Single<Int> {
        return api.putTSState(orderId, status)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }
}