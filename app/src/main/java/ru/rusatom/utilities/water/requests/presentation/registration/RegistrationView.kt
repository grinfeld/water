package ru.rusatom.utilities.water.requests.presentation.registration

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface RegistrationView : MvpView{
    fun showProgress(b: Boolean)
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(it: String)
    fun onServer(serverPath: String)
}