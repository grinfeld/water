package ru.rusatom.utilities.water.requests.model.interactor

import ru.rusatom.utilities.water.requests.entity.request.AuthBody
import ru.rusatom.utilities.water.requests.model.server.Api
import ru.rusatom.utilities.water.requests.model.system.SchedulersProvider
import javax.inject.Inject

class AuthInteractor @Inject constructor(
    private val api: Api,
    private val schedulers: SchedulersProvider
) {

    fun auth(login: String, password: String) = api
        .auth(AuthBody(login, password))
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())

    fun getUserInfo(int: Int) =
        api.getUserInfo(int)
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())

}