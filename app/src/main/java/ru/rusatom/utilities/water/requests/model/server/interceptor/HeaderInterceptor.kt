package ru.rusatom.utilities.water.requests.model.server.interceptor

import ru.rusatom.utilities.water.requests.model.server.TokenInvalidError
import okhttp3.Interceptor
import okhttp3.Response
import ru.rusatom.utilities.water.requests.model.data.storage.Prefs
import javax.inject.Inject

class HeaderInterceptor @Inject constructor(private val prefs: Prefs) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        try {
            request = request.newBuilder()
                .addHeader("Authorization", "Bearer ${prefs.accounts?.internalToken}").build()
        } catch (e: IllegalArgumentException) {
            throw TokenInvalidError()
        }
        return chain.proceed(request)
    }
}