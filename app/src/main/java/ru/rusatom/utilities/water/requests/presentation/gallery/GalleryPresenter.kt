package ru.rusatom.utilities.water.requests.presentation.gallery

import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.model.interactor.GalleryInteractor
import ru.rusatom.utilities.water.requests.model.system.flow.FlowRouter
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import javax.inject.Inject

@InjectViewState
class GalleryPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val galleryInteractor: GalleryInteractor
) : BasePresenter<GalleryView>() {



     fun getPhotos() {
         galleryInteractor.getPhotos()
            // .doOnSubscribe { viewState.showProgress(true) }
            //.doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { viewState.setPhotos(it) },
                { viewState.showMessage(it) }
            )
            .connect()
    }


    fun getVideo(){
        galleryInteractor.getVideo()
            // .doOnSubscribe { viewState.showProgress(true) }
            //.doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { viewState.setPhotos(it) },
                { viewState.showMessage(it) }
            )
            .connect()
    }

    fun onBackPressed() {
        flowRouter.exit()
    }
}