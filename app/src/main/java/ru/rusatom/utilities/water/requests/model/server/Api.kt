package ru.rusatom.utilities.water.requests.model.server

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*
import ru.rusatom.utilities.water.requests.entity.*
import ru.rusatom.utilities.water.requests.entity.OrderTMC.MaterialOrderItems
import ru.rusatom.utilities.water.requests.entity.request.AuthBody
import ru.rusatom.utilities.water.requests.entity.request.WorkBody

interface Api {

    companion object {
        const val API_PATH = "api"
    }

    @POST("$API_PATH/auth")
    fun auth(@Body authBody: AuthBody): Single<AuthData>

    @GET("$API_PATH/employee/{userId}")
    fun getUserInfo(@Path(value = "userId", encoded = true) userId: Int): Single<UserInfo>


    @GET("$API_PATH/transportOrderItem/driver/{userId}")
    fun getTransportOrderItemsById(
        @Path(
            value = "userId",
            encoded = true
        ) userId: Int
    ): Single<List<OrderVehicle>>


    @GET("$API_PATH/task/filter/lead/{userId}")
    fun getTaskById(@Path(value = "userId", encoded = true) userId: Int): Single<List<Task>>

    @GET("$API_PATH/task/{task_id}/materialOrders")
    fun getOrderTMCByTaskId(
        @Path(
            value = "task_id",
            encoded = true
        ) task_id: Int
    ): Single<List<OrderTMC>>

    @GET("$API_PATH/task/{task_id}/transportOrders")
    fun getOrderTSByTaskId(
        @Path(
            value = "task_id",
            encoded = true
        ) task_id: Int
    ): Single<List<OrderTS>>

    @GET("$API_PATH/task/{task_id}/works")
    fun getWorkById(
        @Path(
            value = "task_id",
            encoded = true
        ) task_id: Int
    ): Single<List<Work>>

    @GET("$API_PATH/workname")
    fun getWorks(): Single<List<WorkList>>

    @GET("$API_PATH/taskState")
    fun getTaskState(): Single<List<State>>

    @GET("$API_PATH/transportState")
    fun getTSState(): Single<List<State>>

    @PUT("$API_PATH/TransportOrderItem/{orderItemId}/state")
    fun putTSState(
        @Path(
            value = "orderItemId",
            encoded = true
        ) orderItemId: Int,
        @Body status: Int
    ): Single<Int>

    @PUT("$API_PATH/task/{taskId}/state")
    fun putTaskState(
        @Path(
            value = "taskId",
            encoded = true
        ) taskId: Int,
        @Body status: Int
    ): Single<Int>

    @PUT("$API_PATH/materialOrderItem/{materialOrderItemID}")
    fun putFactCountTMC(
        @Path(
            value = "materialOrderItemID",
            encoded = true
        ) materialOrderItemID: Int,
        @Body materialOrderItem: MaterialOrderItems
    ): Single<MaterialOrderItems>

    @POST("$API_PATH/work/range")
    fun createWork(
        @Body work: List<Work>
    ): Single<Response<Void>>

    @PUT("$API_PATH/work/{workId}")
    fun editWork(
        @Path(
            value = "workId",
            encoded = true
        ) workId: Int,
        @Body work: WorkBody
    ): Single<WorkBody>

    @DELETE("$API_PATH/work/{workId}")
    fun deleteWork(
        @Path(
            value = "workId",
            encoded = true
        ) workId: Int
    ): Single<Response<Void>>

    @PUT("$API_PATH/transportOrderItem/{transportOrderItemId}/setDepartureDt")
    fun setDepartureDt(
        @Path(
            value = "transportOrderItemId",
            encoded = true
        ) transportOrderItemId: Int
    ): Single<OrderTSApprovedDt>

    @PUT("$API_PATH/transportOrderItem/{transportOrderItemId}/setArrivalDt")
    fun setArrivalDt(
        @Path(
            value = "transportOrderItemId",
            encoded = true
        ) transportOrderItemId: Int
    ): Single<OrderTSApprovedDt>

    @DELETE("$API_PATH/transportOrderItem/{transportOrderItemId}/deleteArrivalDt")
    fun resetArrivalDt(
        @Path(
            value = "transportOrderItemId",
            encoded = true
        ) transportOrderItemId: Int
    ): Single<Response<Void>>

    @DELETE("$API_PATH/transportOrderItem/{transportOrderItemId}/deleteDepartureDt")
    fun resetDepartureDt(
        @Path(
            value = "transportOrderItemId",
            encoded = true
        ) transportOrderItemId: Int
    ): Single<Response<Void>>
}