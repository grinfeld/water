package ru.rusatom.utilities.water.requests.ui.global.dialog

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckedTextView
import android.widget.ListView
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_filter_date.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.filter.DateFilter
import ru.rusatom.utilities.water.requests.utils.argument


class FilterDateDialog : DialogFragment() {

    private val listDateFilter: ArrayList<DateFilter> by argument(ARG_LIST_DATE_FILTER)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_filter_date, container)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        list_view.choiceMode = ListView.CHOICE_MODE_MULTIPLE
        list_view.adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_list_item_multiple_choice,
            listDateFilter
        )
        listDateFilter.forEach {
            list_view.setItemChecked(it.type, it.isChecked)
        }
        list_view.setOnItemClickListener { adapterView, view, i, l ->
            val v = view as CheckedTextView
            val currentCheck = v.isChecked
            listDateFilter[i].isChecked = currentCheck
        }
        cancel_action.setOnClickListener {
            dismiss()
        }
        ok_action.setOnClickListener {
            if (listDateFilter.none { dateFilter -> dateFilter.isChecked }) {
                listDateFilter[1].isChecked = true
            }
            val intent = Intent()
            intent.putExtra(ARG_LIST_DATE_FILTER, listDateFilter)
            targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
            dismiss()
        }
    }

    companion object {
        const val ARG_LIST_DATE_FILTER = "arg_list_date"
        fun create(list: ArrayList<DateFilter>) = FilterDateDialog().apply {
            arguments = Bundle().apply {
                putParcelableArrayList(ARG_LIST_DATE_FILTER, list)
            }
        }
    }

}