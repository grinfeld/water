package ru.rusatom.utilities.water.requests.utils

class DateUtil {

    fun millisToTime(millis: Long): String {
        var seconds = 0
        var minutes = 0
        var hours = 0
        if (millis > 1000) {
            seconds = (millis / 1000).toInt()
            if (seconds > 60) {
                minutes = (seconds / 60)
                seconds %= 60
                if (minutes > 60) {
                    hours /= 60
                    minutes %= 60
                }
            }
        }
        return if (hours < 0 || minutes < 0 || seconds < 0) ""
        else if (hours > 0) "${if (hours > 9) "$hours" else "0$hours"}:${if (minutes > 9) "$minutes" else "0$minutes"}:${if (seconds > 9) "$seconds" else "0$seconds"}"
        else if (hours == 0 && minutes > 0) "${if (minutes > 9) "$minutes" else "0$minutes"}:${if (seconds > 9) "$seconds" else "0$seconds"}"
        else if (hours == 0 && minutes == 0) "0:${if (seconds > 9) "$seconds" else "0$seconds"}"
        else ""
    }


}