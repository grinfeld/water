package ru.rusatom.utilities.water.requests.model.interactor


import ru.rusatom.utilities.water.requests.entity.Work

import ru.rusatom.utilities.water.requests.model.server.Api
import ru.rusatom.utilities.water.requests.model.system.SchedulersProvider
import javax.inject.Inject

class WorksReferenceInteractor @Inject constructor(
    private val api: Api,
    private val schedulers: SchedulersProvider
) {

    fun getWorks() = api
        .getWorks()
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())

    fun addWorkList(result: List<Work>) =
        api.createWork(result)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())


}