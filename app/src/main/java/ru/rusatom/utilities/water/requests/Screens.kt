package ru.rusatom.utilities.water.requests

import ru.rusatom.utilities.water.requests.entity.OrderTS.TransportOrderItems
import ru.rusatom.utilities.water.requests.entity.Task
import ru.rusatom.utilities.water.requests.ui.appinfo.AppInfoFragment
import ru.rusatom.utilities.water.requests.ui.filter.FilterFlowFragment
import ru.rusatom.utilities.water.requests.ui.filter.FilterFragment
import ru.rusatom.utilities.water.requests.ui.history.HistoryFragment
import ru.rusatom.utilities.water.requests.ui.main.MainFragment
import ru.rusatom.utilities.water.requests.ui.map.MapFragment
import ru.rusatom.utilities.water.requests.ui.ordervehicle.OrderVehicleFragment
import ru.rusatom.utilities.water.requests.ui.ordervehicle.content.OrderVehicleContentFragment
import ru.rusatom.utilities.water.requests.ui.privacypolicy.PrivacyPolicyFragment
import ru.rusatom.utilities.water.requests.ui.registration.RegistrationFlowFragment
import ru.rusatom.utilities.water.requests.ui.registration.RegistrationFragment
import ru.rusatom.utilities.water.requests.ui.tasks.TasksFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.rusatom.utilities.water.requests.ui.main.MainFlowFragment
import ru.rusatom.utilities.water.requests.ui.notification.NotificationFragment
import ru.rusatom.utilities.water.requests.ui.ordervehicle.OrderVehicleFlowFragment
import ru.rusatom.utilities.water.requests.ui.tasks.TasksFlowFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.TaskContentPagesFlowFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.TaskContentPagesFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.*
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.attachedfiles.AttachedFilesFlowFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.attachedfiles.AttachedFilesFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.attachedfiles.gallery.GalleryFlowFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.attachedfiles.gallery.GalleryFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.tmc.OrderTMCFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.ts.content.OrderTSContentFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.ts.OrderTSFlowFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.ts.OrderTSFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.ts.content.OrderTSContentFlowFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.works.*
import ru.rusatom.utilities.water.requests.ui.techsupport.TechSupportFragment

object Screens {

    // Flows
    object RegistrationFlow : SupportAppScreen() {
        override fun getFragment() = RegistrationFlowFragment()
    }

    object MainFlow : SupportAppScreen() {
        override fun getFragment() = MainFlowFragment()
    }

    data class OrderVehicleContentFlow(
        val orderVehicle: ru.rusatom.utilities.water.requests.entity.OrderVehicle
    ) : SupportAppScreen() {
        override fun getFragment() =
            ru.rusatom.utilities.water.requests.ui.ordervehicle.content.OrderVehicleContentFlow(
                orderVehicle
            )
    }

    data class TaskContentPagesFlow(val task: Task) : SupportAppScreen() {
        override fun getFragment() = TaskContentPagesFlowFragment(task)
    }

    object FilterFlow : SupportAppScreen() {
        override fun getFragment() = FilterFlowFragment()
    }

    data class WorkFlow(val taskId: Int) : SupportAppScreen() {
        override fun getFragment() = WorksFlowFragment(taskId)
    }


    data class WorkReferenceListFlow(val taskId: Int) : SupportAppScreen() {
        override fun getFragment() = WorkReferenceListFlowFragment(taskId)
    }

    data class OrderTSFlow(val taskId: Int) : SupportAppScreen() {
        override fun getFragment() = OrderTSFlowFragment(taskId)
    }

    object AttachedFilesFlow : SupportAppScreen() {
        override fun getFragment() = AttachedFilesFlowFragment()
    }

    data class GalleryFlow(val typeGallery: Int) : SupportAppScreen() {
        override fun getFragment() = GalleryFlowFragment(typeGallery)
    }

    object  OrderVehicleFlow : SupportAppScreen() {
        override fun getFragment() = OrderVehicleFlowFragment()
    }

    object TasksFlow : SupportAppScreen() {
        override fun getFragment() = TasksFlowFragment()
    }

    data class OrderTSContentFlow(val transportOrderItems: TransportOrderItems) : SupportAppScreen() {
        override fun getFragment() = OrderTSContentFlowFragment(transportOrderItems)
    }
    //end region


    // Screens
    object Privacy : SupportAppScreen() {
        override fun getFragment() = PrivacyPolicyFragment()
    }

    object Registration : SupportAppScreen() {
        override fun getFragment() = RegistrationFragment()
    }


    object Main : SupportAppScreen() {
        override fun getFragment() = MainFragment()
    }

    object Tasks : SupportAppScreen() {
        override fun getFragment() = TasksFragment()
    }

    object OrderVehicle : SupportAppScreen() {
        override fun getFragment() = OrderVehicleFragment()
    }

    object Map : SupportAppScreen() {
        override fun getFragment() = MapFragment()
    }

    object AppInfo : SupportAppScreen() {
        override fun getFragment() = AppInfoFragment()
    }

    object Filter : SupportAppScreen() {
        override fun getFragment() = FilterFragment()
    }

    data class OrderVehicleContent(
        val orderVehicle: ru.rusatom.utilities.water.requests.entity.OrderVehicle
    ) : SupportAppScreen() {
        override fun getFragment() = OrderVehicleContentFragment.create(orderVehicle)
    }

    data class TaskContentPages(val task: Task) : SupportAppScreen() {
        override fun getFragment() = TaskContentPagesFragment.create(task)
    }

    data class TaskContent(val task: Task) : SupportAppScreen() {
        override fun getFragment() = TaskContentFragment.create(task)
    }

    data class OrderTMC(val taskId: Int) : SupportAppScreen() {
        override fun getFragment() = OrderTMCFragment.create(taskId)
    }

    data class OrderTS(val taskId: Int) : SupportAppScreen() {
        override fun getFragment() = OrderTSFragment.create(taskId)
    }

    data class Works(val taskId: Int) : SupportAppScreen() {
        override fun getFragment() = WorksFragment.create(taskId)
    }

    object AttachedFiles : SupportAppScreen() {
        override fun getFragment() = AttachedFilesFragment()
    }

    object History : SupportAppScreen() {
        override fun getFragment() = HistoryFragment()
    }

    object Notification : SupportAppScreen(){
        override fun getFragment() = NotificationFragment()
    }

    object TechSupport : SupportAppScreen() {
        override fun getFragment() = TechSupportFragment()
    }

    data class WorkList(val taskId: Int) : SupportAppScreen() {
        override fun getFragment() = WorkReferenceListFragment.create(taskId)
    }

    data class OrderTSContent(val transportOrderItems: TransportOrderItems) : SupportAppScreen() {
        override fun getFragment() = OrderTSContentFragment.create(transportOrderItems)
    }

    data class Gallery(val typeGallery: Int) : SupportAppScreen() {
        override fun getFragment() = GalleryFragment.create(typeGallery)
    }

    //end region
}