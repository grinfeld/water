package ru.rusatom.utilities.water.requests.ui.registration

import android.os.Bundle
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_registration.*
import okhttp3.HttpUrl
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.di.DI
import ru.rusatom.utilities.water.requests.di.module.ServerModule
import ru.rusatom.utilities.water.requests.model.data.storage.Prefs
import ru.rusatom.utilities.water.requests.presentation.registration.RegistrationPresenter
import ru.rusatom.utilities.water.requests.presentation.registration.RegistrationView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.utils.addSystemBottomPadding
import ru.rusatom.utilities.water.requests.utils.addSystemTopPadding
import toothpick.Toothpick
import javax.inject.Inject

class RegistrationFragment : BaseFragment(), RegistrationView, TextView.OnEditorActionListener {
    override val layoutRes = R.layout.fragment_registration

    @InjectPresenter
    lateinit var presenter: RegistrationPresenter

    @ProvidePresenter
    fun providePresenter(): RegistrationPresenter =
        scope.getInstance(RegistrationPresenter::class.java)


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        view?.addSystemTopPadding()
        view?.addSystemBottomPadding()
        server.setOnEditorActionListener(this)
        btn_sign_in.setOnClickListener {
            //presenter.auth(login.text.toString(), password.editText?.text.toString())
            //presenter.auth("komarov", "Pfzdrb8891")
            login()
        }
        presenter.getServer()
    }

    private fun login() {
        val url = server.text.toString()
        if (HttpUrl.parse(url) != null) {
            presenter.auth(login.text.toString(), password.editText?.text.toString(), url)
        } else {
            Toast.makeText(this.context, getString(R.string.invalid_server_url), Toast.LENGTH_SHORT)
                .show()
        }
    }


    override fun showProgress(b: Boolean) {
        showProgressDialog(b)
    }

    override fun showMessage(it: String) {
        Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
    }

    override fun onServer(serverPath: String) {
        server.setText(serverPath)
    }

    override fun onEditorAction(p0: TextView?, actionId: Int, p2: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            login()
            return true
        }
        return false
    }

}
