package ru.rusatom.utilities.water.requests.presentation.ts

import com.arellomobile.mvp.MvpView
import ru.rusatom.utilities.water.requests.entity.OrderTS.TransportOrderItems

interface OrderTSView : MvpView {
    fun showProgress(b: Boolean)
    fun showMessage(it: String)
    fun orderTS(it: List<TransportOrderItems>)
}