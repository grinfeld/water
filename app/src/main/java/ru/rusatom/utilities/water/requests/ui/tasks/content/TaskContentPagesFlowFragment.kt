package ru.rusatom.utilities.water.requests.ui.tasks.content

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.entity.Task
import ru.rusatom.utilities.water.requests.ui.global.FlowFragment

class TaskContentPagesFlowFragment(private val task: Task) : FlowFragment() {
    override fun getLaunchScreen() = Screens.TaskContentPages(task)
}