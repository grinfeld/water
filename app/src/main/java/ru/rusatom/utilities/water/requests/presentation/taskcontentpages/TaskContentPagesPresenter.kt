package ru.rusatom.utilities.water.requests.presentation.taskcontentpages

import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.model.system.flow.FlowRouter
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import javax.inject.Inject

@InjectViewState
class TaskContentPagesPresenter @Inject constructor(
    private val flowRouter: FlowRouter
) : BasePresenter<TaskContentPagesView>() {
    fun onBackPressed() = flowRouter.exit()
}