package ru.rusatom.utilities.water.requests.di.module

import android.content.Context
import com.google.gson.Gson
import ru.rusatom.utilities.water.requests.di.provider.GsonProvider
import ru.rusatom.utilities.water.requests.model.system.AppSchedulers
import ru.rusatom.utilities.water.requests.model.system.ResourceManager
import ru.rusatom.utilities.water.requests.model.system.SchedulersProvider
import ru.rusatom.utilities.water.requests.model.system.message.SystemMessageNotifier
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

import toothpick.config.Module

class AppModule(context: Context) : Module() {
    init {
        // Global
        bind(Context::class.java).toInstance(context)
        bind(SchedulersProvider::class.java).toInstance(AppSchedulers())
        bind(ResourceManager::class.java).singleton()
        bind(SystemMessageNotifier::class.java).toInstance(SystemMessageNotifier())
        bind(Gson::class.java).toProvider(GsonProvider::class.java).providesSingleton()

        // Navigation
        val cicerone = Cicerone.create()
        bind(Router::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)

    }
}