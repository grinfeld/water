package ru.rusatom.utilities.water.requests.presentation.global

import android.annotation.SuppressLint
import android.util.Log
import com.jakewharton.rxrelay2.PublishRelay
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.model.server.ServerError
import ru.rusatom.utilities.water.requests.model.server.TokenInvalidError
import ru.rusatom.utilities.water.requests.model.system.ResourceManager
import ru.rusatom.utilities.water.requests.model.system.SchedulersProvider
import ru.rusatom.utilities.water.requests.model.system.message.SystemMessageNotifier
import ru.rusatom.utilities.water.requests.utils.userMessage
import ru.terrakok.cicerone.Router
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ErrorHandler @Inject constructor(
    private val router: Router,
    private val systemMessageNotifier: SystemMessageNotifier,
    private val resourceManager: ResourceManager,
    private val schedulers: SchedulersProvider
) {

    private val authErrorRelay = PublishRelay.create<Boolean>()

    init {
        subscribeOnAuthErrors()
    }

    fun proceed(error: Throwable, messageListener: (String) -> Unit = {}) {
        Timber.e(error)
        when (error) {
            is ServerError -> when (error.errorCode) {
                401 -> authErrorRelay.accept(true)
                else -> messageListener(error.userMessage(resourceManager))
            }
            is TokenInvalidError -> authErrorRelay.accept(true)
            else -> messageListener(error.userMessage(resourceManager))
        }
    }

    @SuppressLint("CheckResult")
    private fun subscribeOnAuthErrors() {
        authErrorRelay
            .throttleFirst(50, TimeUnit.MILLISECONDS)
            .observeOn(schedulers.ui())
            .subscribe { logout() }
    }

    private fun logout() {
        systemMessageNotifier.send(resourceManager.getString(R.string.auto_logout_message))
        router.newRootScreen(Screens.RegistrationFlow)
    }
}