package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.works

import android.os.Bundle
import android.view.View
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.util.forEach
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_work_reference_list.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.WorkList
import ru.rusatom.utilities.water.requests.presentation.worklist.WorkReferenceListPresenter
import ru.rusatom.utilities.water.requests.presentation.worklist.WorkReferenceListView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.utils.addSystemBottomPadding
import ru.rusatom.utilities.water.requests.utils.addSystemTopPadding
import ru.rusatom.utilities.water.requests.utils.argument


class WorkReferenceListFragment : BaseFragment(), WorkReferenceListView,
    SearchView.OnQueryTextListener {

    override val layoutRes = R.layout.fragment_work_reference_list

    @InjectPresenter
    lateinit var presenter: WorkReferenceListPresenter

    @ProvidePresenter
    fun providePresenter(): WorkReferenceListPresenter =
        scope.getInstance(WorkReferenceListPresenter::class.java)

    private val taskId: Int by argument(ARG_TASK_ID)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        view?.addSystemBottomPadding()
        toolbar.apply {
            addSystemTopPadding()
            toolbar.setNavigationOnClickListener { presenter.onBackPressed() }
            inflateMenu(R.menu.work_add_menu)
            val searchView = (menu.findItem(R.id.action_search).actionView as SearchView)
            searchView.queryHint = getString(R.string.hint_search)
            searchView.setOnQueryTextListener(this@WorkReferenceListFragment)
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_done -> {

                    }
                }
                true
            }
        }
        presenter.getWorks()
    }


    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    private fun getChecked(): List<WorkList> {
        val result = mutableListOf<WorkList>()
        list_view.checkedItemPositions.forEach { key, value ->
            val item = list_view.adapter.getItem(key) as WorkList
            result.add(item)
        }
        return result
    }

    override fun showProgress(b: Boolean) {
        toolbar_progress_bar.visibility = if (b) {
            View.VISIBLE
        } else {
            View.INVISIBLE
        }
    }

    override fun setWorks(it: List<WorkList>) {
        list_view.choiceMode = ListView.CHOICE_MODE_MULTIPLE
        list_view.adapter = WorkReferenceAdapter(
            requireContext(), android.R.layout.simple_list_item_multiple_choice, it
        )
    }

    override fun showMessage(it: String) {
        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
    }

    companion object {
        private const val ARG_TASK_ID = "arg_task_id"
        fun create(taskId: Int) =
            WorkReferenceListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_TASK_ID, taskId)
                }
            }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return false
    }

}