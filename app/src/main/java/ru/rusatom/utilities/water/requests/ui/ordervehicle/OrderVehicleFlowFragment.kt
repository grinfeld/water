package ru.rusatom.utilities.water.requests.ui.ordervehicle

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.ui.global.FlowFragment

class OrderVehicleFlowFragment : FlowFragment(){

    override fun getLaunchScreen() = Screens.OrderVehicle


}