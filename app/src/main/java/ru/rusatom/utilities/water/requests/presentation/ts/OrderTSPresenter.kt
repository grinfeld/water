package ru.rusatom.utilities.water.requests.presentation.ts

import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.entity.OrderTS.TransportOrderItems
import ru.rusatom.utilities.water.requests.model.interactor.OrderTSInteractor
import ru.rusatom.utilities.water.requests.model.system.flow.FlowRouter
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import ru.rusatom.utilities.water.requests.presentation.global.ErrorHandler
import javax.inject.Inject

@InjectViewState
class OrderTSPresenter @Inject constructor(
    private val orderTSInteractor: OrderTSInteractor,
    private val errorHandler: ErrorHandler,
    private val flowRouter: FlowRouter
) : BasePresenter<OrderTSView>() {

    fun getTS(taskId: Int) {
        orderTSInteractor.getTS(taskId)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                {
                    val transportOrderItems = mutableListOf<TransportOrderItems>()
                    it.forEach { ots ->
                        ots.transportOrderItems.forEach { ts ->
                            transportOrderItems.add(ts)
                        }
                    }
                    viewState.orderTS(transportOrderItems)
                },
                { throwable -> errorHandler.proceed(throwable) { viewState.showMessage(it) } }
            )
            .connect()
    }

    fun openTSItemContent(t: TransportOrderItems) {
        flowRouter.startFlow(Screens.OrderTSContentFlow(t))
    }


}