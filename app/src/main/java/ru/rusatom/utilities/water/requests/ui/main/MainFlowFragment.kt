package ru.rusatom.utilities.water.requests.ui.main

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.ui.global.FlowFragment

class MainFlowFragment : FlowFragment(){

    override fun getLaunchScreen() = Screens.Main

}