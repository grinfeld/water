package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.ts

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_order_tmc.recycler
import kotlinx.android.synthetic.main.fragment_order_tmc.swipeContainer
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.OrderTS.TransportOrderItems
import ru.rusatom.utilities.water.requests.presentation.ts.OrderTSPresenter
import ru.rusatom.utilities.water.requests.presentation.ts.OrderTSView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.adapters.OrderTSAdapter
import ru.rusatom.utilities.water.requests.utils.addSystemBottomPadding
import ru.rusatom.utilities.water.requests.utils.argument

class OrderTSFragment : BaseFragment(), OrderTSView, SwipeRefreshLayout.OnRefreshListener {

    @InjectPresenter
    lateinit var presenter: OrderTSPresenter

    @ProvidePresenter
    fun providePresenter(): OrderTSPresenter =
        scope.getInstance(OrderTSPresenter::class.java)

    override val layoutRes = R.layout.fragment_order_ts

    private val taskId: Int by argument(ARG_TASK_ID)

    private lateinit var orderTSAdapter: OrderTSAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        view?.addSystemBottomPadding()
        orderTSAdapter = OrderTSAdapter(this::orderTSItemClickListener)
        recycler.layoutManager = LinearLayoutManager(requireContext())
        recycler.adapter = orderTSAdapter
        presenter.getTS(taskId)
        swipeContainer.setOnRefreshListener(this)
    }

    private fun orderTSItemClickListener(t: TransportOrderItems) {
       presenter.openTSItemContent(t)
    }

    companion object {
        private const val ARG_TASK_ID = "arg_task_id"
        fun create(taskId: Int) =
            OrderTSFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_TASK_ID, taskId)
                }
            }
    }

    override fun showProgress(b: Boolean) {
        swipeContainer.isRefreshing = b
    }

    override fun showMessage(it: String) {

    }

    override fun orderTS(it: List<TransportOrderItems>) {
        orderTSAdapter.submitList(it)
    }

    override fun onRefresh() {
        presenter.getTS(taskId)
    }


}