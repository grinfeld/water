package ru.rusatom.utilities.water.requests.di.provider

import com.google.gson.Gson
import ru.rusatom.utilities.water.requests.di.ServerPath
import ru.rusatom.utilities.water.requests.di.WithErrorHandler
import ru.rusatom.utilities.water.requests.model.server.Api
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Provider

class ApiProvider @Inject constructor(
        @WithErrorHandler private val okHttpClient: OkHttpClient,
        private val gson: Gson,
        @ServerPath private val serverPath: String
) : Provider<Api> {


    private fun getOriginalApi() =
            Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .baseUrl(serverPath)
                    .build()
                    .create(Api::class.java)

    override fun get(): Api = getOriginalApi()
}
