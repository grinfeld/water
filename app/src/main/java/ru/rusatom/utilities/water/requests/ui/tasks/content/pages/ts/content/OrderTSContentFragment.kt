package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.ts.content

import android.os.Bundle
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_order_ts_content.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.OrderTS.TransportOrderItems
import ru.rusatom.utilities.water.requests.presentation.tscontent.OrderTSContentPresenter
import ru.rusatom.utilities.water.requests.presentation.tscontent.OrderTSContentView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.utils.addSystemBottomPadding
import ru.rusatom.utilities.water.requests.utils.addSystemTopPadding
import ru.rusatom.utilities.water.requests.utils.argument

class OrderTSContentFragment : BaseFragment(), OrderTSContentView {

    @InjectPresenter
    lateinit var presenter: OrderTSContentPresenter

    @ProvidePresenter
    fun providePresenter(): OrderTSContentPresenter =
        scope.getInstance(OrderTSContentPresenter::class.java)

    override val layoutRes = R.layout.fragment_order_ts_content

    private val ts: TransportOrderItems by argument(ARG_TS_ITEM)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        view?.addSystemBottomPadding()
        toolbar.apply {
            addSystemTopPadding()
            title = "Заказ ТС №${ts.id}"
            setNavigationOnClickListener { presenter.onBackPressed() }
        }
        address.text = ts.demandAddress
        date_start.text = ts.demandStartDt
        date_end.text = ts.demandEndDt
        status.text = ts.transportStateName
        arrivalDt.text = ts.arrivalDt
        departureDt.text = ts.departureDt
        refreshBtn()
        btn_arrival.setOnClickListener {
            if (arrivalDt.text.isEmpty()) presenter.setArrivalDt(ts.id) else presenter.resetArrivalDt(ts.id)
        }
        btn_departure.setOnClickListener {
            if (departureDt.text.isEmpty()) presenter.setDepartureDt(ts.id) else presenter.resetDepartureDt(ts.id)
        }
    }

    private fun refreshBtn(){
        btn_arrival.text =
            if (arrivalDt.text.isEmpty()) getString(R.string.approved_arrival) else getString(R.string.reset_arrival)
        btn_departure.text =
            if (departureDt.text.isEmpty()) getString(R.string.approved_departure) else getString(R.string.reset_departure)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        presenter.onBackPressed()
    }


    companion object {
        private const val ARG_TS_ITEM = "arg_ts_item"
        fun create(transportOrderItems: TransportOrderItems) =
            OrderTSContentFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_TS_ITEM, transportOrderItems)
                }
            }
    }

    override fun showProgress(b: Boolean) {
        showProgressDialog(b)
    }

    override fun showMessage(it: String) {
        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
    }

    override fun onArrivalDt(it: String) {
        arrivalDt.text = it
        refreshBtn()
    }

    override fun onDepartureDt(it: String) {
        departureDt.text = it
        refreshBtn()
    }


}