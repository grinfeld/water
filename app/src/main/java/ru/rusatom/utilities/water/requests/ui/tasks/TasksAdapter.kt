package ru.rusatom.utilities.water.requests.ui.tasks

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_nav_drawer.view.*
import kotlinx.android.synthetic.main.item_task.view.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.Task
import ru.rusatom.utilities.water.requests.utils.getAddress
import ru.rusatom.utilities.water.requests.utils.getNames
import ru.rusatom.utilities.water.requests.utils.inflate

typealias OpenTaskContentClickListener = (Task) -> Unit

class TasksAdapter(
    private val openTaskContentClickListener: OpenTaskContentClickListener
) : RecyclerView.Adapter<TasksAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.context.inflate(R.layout.item_task, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val task = differ.currentList[position]
        holder.bindView(task)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class ViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {
        fun bindView(task: Task) = with(itemView) {
            txtName.text = task.taskTypeName
            type_task.text = task.id.toString()
            related_req.text = task.requestId.toString()
            txt_object.text = task.devices.getNames()
            addressTask.text = task.devices.getAddress()
            plan_start.text = task.planStartDt
            plan_end.text = task.planEndDt
            status.text = task.taskStateName
            itemView.setOnClickListener {
                openTaskContentClickListener(task)
            }
        }
    }


    private val diffCallback = object : DiffUtil.ItemCallback<Task>() {
        override fun areItemsTheSame(oldItem: Task, newItem: Task): Boolean {
            return oldItem.id == newItem.id &&
                    oldItem.taskStateId == newItem.taskStateId &&
                    oldItem.requestId == newItem.requestId &&
                    oldItem.planStartDt == newItem.planStartDt &&
                    oldItem.planEndDt == newItem.planEndDt
        }

        override fun areContentsTheSame(oldItem: Task, newItem: Task): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    fun submitList(task: List<Task>) = differ.submitList(task)
}