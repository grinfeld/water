package ru.rusatom.utilities.water.requests.presentation.tasks

import com.arellomobile.mvp.MvpView
import ru.rusatom.utilities.water.requests.entity.Task

interface TasksView : MvpView {
    fun showProgress(b: Boolean)
    fun showMessage(it: String)
    fun onTasks(it: List<Task>)
}