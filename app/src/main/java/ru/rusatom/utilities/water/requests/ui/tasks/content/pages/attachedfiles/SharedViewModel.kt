package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.attachedfiles

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.rusatom.utilities.water.requests.entity.MediaData


class SharedViewModel : ViewModel() {

    private val mediaData = MutableLiveData<List<MediaData>>()

    fun setData(mediaData: List<MediaData>){
        this.mediaData.value = mediaData
    }

    fun getData(): LiveData<List<MediaData>>{
        return mediaData
    }
}