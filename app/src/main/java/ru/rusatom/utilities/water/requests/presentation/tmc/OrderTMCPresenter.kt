package ru.rusatom.utilities.water.requests.presentation.tmc

import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.entity.OrderTMC.MaterialOrderItems
import ru.rusatom.utilities.water.requests.model.interactor.OrderTMCInteractor
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import ru.rusatom.utilities.water.requests.presentation.global.ErrorHandler
import javax.inject.Inject

@InjectViewState
class OrderTMCPresenter @Inject constructor(
    private val orderTMCInteractor: OrderTMCInteractor,
    private val errorHandler: ErrorHandler
) : BasePresenter<OrderTMCView>() {


    fun getTmc(taskId: Int) {
        orderTMCInteractor.getTMC(taskId)
            .doOnSubscribe { viewState.showRefreshProgress(true) }
            .doAfterTerminate { viewState.showRefreshProgress(false) }
            .subscribe(
                {
                    val materialOrdItms = mutableListOf<MaterialOrderItems>()
                    it.forEach { tmc ->
                        tmc.materialOrderItems.forEach { materialOrdItm ->
                            materialOrdItm.stateName = tmc.stateName
                            materialOrdItms.add(materialOrdItm)
                        }
                    }
                    viewState.orderTMC(materialOrdItms)
                },
                { throwable -> errorHandler.proceed(throwable) { viewState.showMessage(it) } }
            )
            .connect()
    }

    fun putFactCont(materialOrderItems: MaterialOrderItems) {
        orderTMCInteractor.putFactCount(materialOrderItems)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { viewState.onFactContChange() },
                { throwable -> errorHandler.proceed(throwable) { viewState.showMessage(it) } }
            )
            .connect()
    }


}