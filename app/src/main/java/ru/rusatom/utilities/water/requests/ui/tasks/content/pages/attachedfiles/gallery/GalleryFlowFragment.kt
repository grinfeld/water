package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.attachedfiles.gallery

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.ui.global.FlowFragment

class GalleryFlowFragment(private val typeGallery: Int) : FlowFragment() {

    override fun getLaunchScreen() = Screens.Gallery(typeGallery)
}