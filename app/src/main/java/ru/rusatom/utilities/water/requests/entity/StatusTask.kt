package ru.rusatom.utilities.water.requests.entity

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import ru.rusatom.utilities.water.requests.R

enum class StatusTask(
    val status: Int,
    @StringRes val strName: Int,
    @DrawableRes val drawableRes: Int
) {

    NEW(0, R.string.tasks_status_new, R.drawable.bg_status_new),
    APPOINTED(1, R.string.tasks_status_appointed, R.drawable.bg_status_appointed),
    CONFIRMED(2, R.string.tasks_status_confirmed, R.drawable.bg_status_confirmed),
    WORK(3, R.string.tasks_status_in_work, R.drawable.bg_status_in_work),
    WAIT(4, R.string.tasks_status_wait, R.drawable.bg_status_wait),
    COMPLETE(5, R.string.tasks_status_complete, R.drawable.bg_status_complete)
}