package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.attachedfiles

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_attached_files.*
import kotlinx.android.synthetic.main.fragment_attached_files.recycler
import kotlinx.android.synthetic.main.fragment_works.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.MediaData
import ru.rusatom.utilities.water.requests.presentation.attachedfiles.AttachedFilesPresenter
import ru.rusatom.utilities.water.requests.presentation.attachedfiles.AttachedFilesView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.adapters.AttachFileAdapter
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.adapters.WorksAdapter
import ru.rusatom.utilities.water.requests.utils.addSystemBottomPadding
import java.util.*
import kotlin.collections.ArrayList


class AttachedFilesFragment : BaseFragment(), AttachedFilesView {

    override val layoutRes = R.layout.fragment_attached_files

    @InjectPresenter
    lateinit var presenter: AttachedFilesPresenter

    @ProvidePresenter
    fun providePresenter(): AttachedFilesPresenter =
        scope.getInstance(AttachedFilesPresenter::class.java)

    lateinit var attachFileAdapter: AttachFileAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val svm = ViewModelProviders.of(requireActivity()).get(
            SharedViewModel::class.java
        )
        svm.getData().observe(this, androidx.lifecycle.Observer {
            attachFileAdapter = AttachFileAdapter(requireContext(), it)
            recycler.layoutManager = LinearLayoutManager(requireContext())
            recycler.adapter = attachFileAdapter
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        view?.addSystemBottomPadding()
        fab_l.setMainFabOnClickListener {
            if (fab_l.isOptionsMenuOpened) {
                fab_l.closeOptionsMenu()
            }
        }
        fab_l.setMiniFabSelectedListener {
            when (it.itemId) {
                R.id.action_attach_photo -> {
                    presenter.openGallery(PHOTO)
                }
                R.id.action_attach_video -> {
                    presenter.openGallery(VIDEO)
                }
            }
        }
    }


    companion object {
        const val PHOTO = 1
        const val VIDEO = 2
    }

}