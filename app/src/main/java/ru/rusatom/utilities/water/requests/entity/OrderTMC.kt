package ru.rusatom.utilities.water.requests.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderTMC(
    @SerializedName("id") val id: Int,
    @SerializedName("taskId") val taskId: Int,
    @SerializedName("orderStateName") val stateName: String,
    @SerializedName("materialOrderItems") val materialOrderItems: List<MaterialOrderItems>
) : Parcelable {
    @Parcelize
    data class MaterialOrderItems(
        @SerializedName("id") val id: Int,
        @SerializedName("materialId") val materialId: Int,
        @SerializedName("materialName") val materialName: String,
        @SerializedName("planQuantity") val planQuantity: Float,
        @SerializedName("factQuantity") var factQuantity: Float,
        @SerializedName("materialOrderId") val materialOrderId: Int,
        var stateName: String,
        @SerializedName("material") val material: Material
    ) : Parcelable

    @Parcelize
    data class Material(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("description") val description: String,
        @SerializedName("unitId") val unitId: Int,
        @SerializedName("availableQuantity") val availableQuantity: Int,
        @SerializedName("unit") val unit: Unit
    ) : Parcelable

    @Parcelize
    data class Unit(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("shortName") val shortName: String,
        @SerializedName("description") val description: String
    ) : Parcelable
}