package ru.rusatom.utilities.water.requests.entity

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import ru.rusatom.utilities.water.requests.R

enum class StatusTaskOrderTMC(
    val status: Int,
    @StringRes val strName: Int,
    @DrawableRes val drawableRes: Int
) {

    NEW(0, R.string.tasks_order_ts_status_new, R.drawable.bg_status_new),
    PROCESSING(1, R.string.tasks_order_ts_in_processing, R.drawable.bg_status_processing),
    REJECTED(2, R.string.tasks_order_ts_rejected, R.drawable.bg_status_rejected),
    PROVIDED(3, R.string.tasks_order_ts_provided, R.drawable.bg_status_provided)

}