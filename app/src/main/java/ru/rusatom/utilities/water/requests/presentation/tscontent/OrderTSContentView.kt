package ru.rusatom.utilities.water.requests.presentation.tscontent

import com.arellomobile.mvp.MvpView

interface OrderTSContentView : MvpView {
    fun showProgress(b: Boolean)
    fun showMessage(it: String)
    fun onArrivalDt(it: String)
    fun onDepartureDt(it: String)
}