package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.works

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import kotlinx.android.synthetic.main.fragment_works.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.Work
import ru.rusatom.utilities.water.requests.presentation.work.WorkPresenter
import ru.rusatom.utilities.water.requests.presentation.work.WorkView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.ui.global.dialog.ConfirmationDelDialog
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.adapters.WorksAdapter
import ru.rusatom.utilities.water.requests.utils.addSystemBottomPadding
import ru.rusatom.utilities.water.requests.utils.argument
import java.text.SimpleDateFormat
import java.util.*

class WorksFragment : BaseFragment(), WorkView, SwipeRefreshLayout.OnRefreshListener {

    override val layoutRes = R.layout.fragment_works

    private lateinit var worksAdapter: WorksAdapter

    private val taskId: Int by argument(ARG_TASK_ID)

    @InjectPresenter
    lateinit var presenter: WorkPresenter

    @ProvidePresenter
    fun providePresenter(): WorkPresenter =
        scope.getInstance(WorkPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        view?.addSystemBottomPadding()
        worksAdapter = WorksAdapter(this::delWork, this::editWork)
        recycler.layoutManager = LinearLayoutManager(requireContext())
        recycler.adapter = worksAdapter
        swipeContainer.setOnRefreshListener(this)
        presenter.getWorkById(taskId)
        fabAdd.setOnClickListener {
            presenter.openAddWork(taskId)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val work = data?.getParcelableExtra<Work>(ConfirmationDelDialog.ARG_WORK_PARC)
        work?.let { presenter.delWork(it) }
    }

    private fun delWork(work: Work) {
        val dialog = ConfirmationDelDialog.create(work)
        dialog.setTargetFragment(this, 1)
        dialog.show(parentFragmentManager, null)
    }

    private fun editWork(work: Work) {
        SingleDateAndTimePickerDialog.Builder(requireActivity())
            .title(getString(R.string.date_time_complete_work))
            .mainColor(requireContext().getColor(R.color.colorAccent))
            .listener {
                val dateEnd = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault()).format(it)
                work.dateEnd = dateEnd
                presenter.editWork(work)
            }.display()
    }

    companion object {
        private const val ARG_TASK_ID = "arg_task_id"
        fun create(taskId: Int) =
            WorksFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_TASK_ID, taskId)
                }
            }
    }

    override fun showProgress(b: Boolean) {
        swipeContainer.isRefreshing = b
    }

    override fun showMessage(it: String) {

    }

    override fun onWorks(it: List<Work>) {
        worksAdapter.submitList(it)
    }

    override fun onDelWork() {
        onRefresh()
    }

    override fun onEditWork() {
        onRefresh()
    }

    override fun onRefresh() {
        presenter.getWorkById(taskId)
    }

}