package ru.rusatom.utilities.water.requests.ui.tasks.content

import android.os.Bundle
import androidx.fragment.app.FragmentPagerAdapter
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_map.toolbar
import kotlinx.android.synthetic.main.fragment_task_content_pages.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.entity.Task
import ru.rusatom.utilities.water.requests.presentation.taskcontentpages.TaskContentPagesPresenter
import ru.rusatom.utilities.water.requests.presentation.taskcontentpages.TaskContentPagesView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.utils.addSystemTopPadding
import ru.rusatom.utilities.water.requests.utils.argument

class TaskContentPagesFragment : BaseFragment(), TaskContentPagesView {

    override val layoutRes = R.layout.fragment_task_content_pages

    @InjectPresenter
    lateinit var presenter: TaskContentPagesPresenter

    @ProvidePresenter
    fun providePresenter(): TaskContentPagesPresenter =
        scope.getInstance(TaskContentPagesPresenter::class.java)

    private val task: Task by argument(ARG_TASK)
    private val adapter: TaskContentPagesAdapter by lazy { TaskContentPagesAdapter() }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.apply {
            addSystemTopPadding()
            toolbar.setNavigationOnClickListener { presenter.onBackPressed() }
            title = task.taskTypeName
        }
        viewPager.adapter = adapter
    }

    companion object {
        private const val ARG_TASK = "arg_task"
        fun create(task: Task) =
            TaskContentPagesFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_TASK, task)
                }
            }
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    private inner class TaskContentPagesAdapter : FragmentPagerAdapter(
        childFragmentManager,
        BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {
        override fun getItem(position: Int) = when (position) {
            0 -> Screens.TaskContent(task).fragment
            1 -> Screens.OrderTMC(task.id).fragment
            2 -> Screens.OrderTSFlow(task.id).fragment
            3 -> Screens.WorkFlow(task.id).fragment
            4 -> Screens.AttachedFilesFlow.fragment
            else -> Screens.TaskContent(task).fragment
        }

        override fun getCount() = 5

        override fun getPageTitle(position: Int) = when (position) {
            0 -> getString(R.string.content)
            1 -> getString(R.string.orders_tmc)
            2 -> getString(R.string.orders_ts)
            3 -> getString(R.string.works)
            4 -> getString(R.string.attached_files)
            else -> getString(R.string.content)
        }
    }

}