package ru.rusatom.utilities.water.requests.presentation.taskcontent

import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.model.interactor.TasksContentInteractor
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import ru.rusatom.utilities.water.requests.presentation.global.ErrorHandler
import ru.rusatom.utilities.water.requests.presentation.ordervehiclecontent.OrderVehicleContentView
import javax.inject.Inject

@InjectViewState
class TaskContentPresenter @Inject constructor(
    private val tasksContentInteractor: TasksContentInteractor,
    private val errorHandler: ErrorHandler
) : BasePresenter<TaskContentView>() {

    fun changeState(orderId: Int, status: Int) {
        tasksContentInteractor.changeStatus(orderId, status)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                {
                    viewState.changeStatus(it)
                },
                { throwable -> errorHandler.proceed(throwable) { viewState.showMessage(it) } }
            )
            .connect()
    }
}