package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_order_ts.view.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.OrderTS.TransportOrderItems
import ru.rusatom.utilities.water.requests.utils.*

typealias OrderTSItemClickListener = (TransportOrderItems) -> Unit

class OrderTSAdapter(private val orderTSItemClickListener: OrderTSItemClickListener) :
    RecyclerView.Adapter<OrderTSAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.context.inflate(R.layout.item_order_ts, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(h: ViewHolder, position: Int) {
        val o = differ.currentList[position]
        h.bindView(o)
    }


    override fun getItemCount(): Int {
        return differ.currentList.size
    }


    inner class ViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {
        fun bindView(o: TransportOrderItems) = with(itemView) {
            txtName.text = "Заказ ТС №${o.id}"
            date_start.text = o.demandStartDt
            date_end.text = o.demandEndDt
            address_arrival.text = o.demandAddress
            status.text = o.transportStateName
            itemView.setOnClickListener {
                orderTSItemClickListener(o)
            }
        }
    }

    private val diffCallback = object : DiffUtil.ItemCallback<TransportOrderItems>() {
        override fun areItemsTheSame(
            oldItem: TransportOrderItems,
            newItem: TransportOrderItems
        ): Boolean {
            return oldItem.id == newItem.id &&
                    oldItem.vehicleTypeId == newItem.vehicleTypeId &&
                    oldItem.vehicleTypeName == newItem.vehicleTypeName &&
                    oldItem.transportStateId == newItem.transportStateId &&
                    oldItem.demandEndDt == newItem.demandEndDt
        }

        override fun areContentsTheSame(
            oldItem: TransportOrderItems,
            newItem: TransportOrderItems
        ): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    fun submitList(tso: List<TransportOrderItems>) = differ.submitList(tso)

}