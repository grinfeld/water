package ru.rusatom.utilities.water.requests.di

import javax.inject.Qualifier


@Qualifier
annotation class DatabaseName

@Qualifier
annotation class ServerPath

@Qualifier
annotation class WithErrorHandler


