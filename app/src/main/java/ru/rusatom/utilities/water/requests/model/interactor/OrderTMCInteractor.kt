package ru.rusatom.utilities.water.requests.model.interactor

import io.reactivex.Single
import ru.rusatom.utilities.water.requests.entity.OrderTMC
import ru.rusatom.utilities.water.requests.entity.OrderTMC.MaterialOrderItems
import ru.rusatom.utilities.water.requests.model.server.Api
import ru.rusatom.utilities.water.requests.model.system.SchedulersProvider
import javax.inject.Inject

class OrderTMCInteractor @Inject constructor(
    private val api: Api,
    private val schedulers: SchedulersProvider
) {
    fun getTMC(taskId: Int): Single<List<OrderTMC>> {
        return api.getOrderTMCByTaskId(taskId)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

    fun putFactCount(materialOrderItems: MaterialOrderItems): Single<MaterialOrderItems> {
        return api.putFactCountTMC(materialOrderItems.id, materialOrderItems)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

}