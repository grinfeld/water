package ru.rusatom.utilities.water.requests.model.system.message

enum class SystemMessageType {
    ALERT,
    TOAST
}