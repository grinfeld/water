package ru.rusatom.utilities.water.requests.presentation.ordervehicle

import com.arellomobile.mvp.MvpView
import ru.rusatom.utilities.water.requests.entity.OrderVehicle

interface OrderVehicleView : MvpView {
    fun showProgress(b: Boolean)
    fun orderTransports(it: List<OrderVehicle>?)
    fun showMessage(it: String)
}