package ru.rusatom.utilities.water.requests.ui.tasks.content.pages

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_task_content.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.Task
import ru.rusatom.utilities.water.requests.entity.status.StatusImpl
import ru.rusatom.utilities.water.requests.presentation.taskcontent.TaskContentPresenter
import ru.rusatom.utilities.water.requests.presentation.taskcontent.TaskContentView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.utils.*

class TaskContentFragment : BaseFragment(), TaskContentView {

    override val layoutRes = R.layout.fragment_task_content

    @InjectPresenter
    lateinit var presenter: TaskContentPresenter

    @ProvidePresenter
    fun providePresenter(): TaskContentPresenter =
        scope.getInstance(TaskContentPresenter::class.java)


    private val task: Task by argument(ARG_TASK)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        view?.addSystemBottomPadding()
        type_task.text = task.id.toString()
        related_req.text = task.requestId.toString()
        txt_object.text = task.devices.getNames()
        address.text = task.devices.getAddress()
        date_start.text = task.planStartDt
        date_end.text = task.planEndDt
        status.text = task.taskStateName
//        status.setOnClickListener {
//            StatusDialog.create(StatusImpl.TASK)
//                .show(requireActivity().supportFragmentManager, null)
//        }
        btn_state_plus.setOnClickListener {
            if (task.taskStateId == 4) {
                presenter.changeState(task.id, 6)
            } else {
                presenter.changeState(task.id, task.taskStateId + 1)
            }

        }
        btn_state_minus.setOnClickListener {
            when (task.taskStateId) {
                4 -> {
                    presenter.changeState(task.id, 5)
                }
                5 -> {
                    presenter.changeState(task.id, 4)
                }
                else -> {
                    presenter.changeState(task.id, 7)
                }
            }
        }
        asd(task.taskStateId)
    }

    private fun asd(stateId: Int) {
        when (stateId) {
            1 -> {
                btn_container.visibility = View.VISIBLE
                btn_state_plus.visibility = View.GONE
                btn_state_minus.visibility = View.VISIBLE
                btn_state_minus.text = getString(R.string.task_status_cancel_btn)
            }
            2 -> {
                btn_container.visibility = View.VISIBLE
                btn_state_plus.visibility = View.VISIBLE
                btn_state_minus.visibility = View.VISIBLE
                btn_state_plus.text = getString(R.string.tasks_status_confirmed_btn)
                btn_state_minus.text = getString(R.string.task_status_cancel_btn)
            }
            3 -> {
                btn_container.visibility = View.VISIBLE
                btn_state_plus.visibility = View.VISIBLE
                btn_state_minus.visibility = View.VISIBLE
                btn_state_plus.text = getString(R.string.tasks_status_in_work)
                btn_state_minus.text = getString(R.string.task_status_cancel_btn)
            }
            4 -> {
                btn_container.visibility = View.VISIBLE
                btn_state_plus.visibility = View.VISIBLE
                btn_state_minus.visibility = View.VISIBLE
                btn_state_plus.text = getString(R.string.tasks_status_complete_btn)
                btn_state_minus.text = getString(R.string.tasks_status_wait)
            }
            5 -> {
                btn_container.visibility = View.VISIBLE
                btn_state_plus.visibility = View.GONE
                btn_state_minus.visibility = View.VISIBLE
                btn_state_minus.text = getString(R.string.tasks_status_in_work)
            }
            6 -> {
                btn_container.visibility = View.GONE
            }
            7 -> {
                btn_container.visibility = View.GONE
            }
        }
    }

    companion object {
        private const val ARG_TASK = "arg_task"
        fun create(task: Task) =
            TaskContentFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_TASK, task)
                }
            }
    }

    override fun showProgress(b: Boolean) {
        showProgressDialog(b)
    }

    override fun changeStatus(it: Int) {
        asd(it)
        task.taskStateId = it
        status.text = StatusImpl.TASK.text(it - 1, requireContext())
    }

    override fun showMessage(it: String) {

    }

}