package ru.rusatom.utilities.water.requests.presentation.ordervehicle

import android.text.format.DateUtils
import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.entity.OrderVehicle
import ru.rusatom.utilities.water.requests.model.data.storage.Prefs
import ru.rusatom.utilities.water.requests.model.interactor.AuthInteractor
import ru.rusatom.utilities.water.requests.model.interactor.OrderVehicleInteractor
import ru.rusatom.utilities.water.requests.model.system.flow.FlowRouter
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import ru.rusatom.utilities.water.requests.presentation.global.ErrorHandler
import ru.rusatom.utilities.water.requests.utils.toDateDay
import java.util.*
import javax.inject.Inject

@InjectViewState
class OrderVehiclePresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val errorHandler: ErrorHandler,
    private val orderVehicleInteractor: OrderVehicleInteractor,
    private val prefs: Prefs
) : BasePresenter<OrderVehicleView>() {
    fun openOrderVehicleContent(orderVehicle: OrderVehicle) {
        flowRouter.startFlow(Screens.OrderVehicleContentFlow(orderVehicle))
    }

    fun openFilter() {
        flowRouter.startFlow(Screens.FilterFlow)
    }

    fun getOrderTransports() {
        orderVehicleInteractor.getOrderTSItems()
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                {
                    val dateFilter = prefs.dateFilter.filter { dateFilter -> dateFilter.isChecked }
                    val all = dateFilter.firstOrNull() { dF -> dF.type == 0 }
                    val week = dateFilter.firstOrNull() { dF -> dF.type == 2 }
                    val mouth = dateFilter.firstOrNull() { dF -> dF.type == 3 }
                    when {
                        all != null -> {
                            viewState.orderTransports(it)
                        }
                        mouth != null -> {
                            val m = Calendar.getInstance()
                            m.add(Calendar.MONTH, 1)
                            viewState.orderTransports(it.filter { task ->
                                task.startDate.toDateDay()
                                    .after(Date()) && task.startDate.toDateDay().before(m.time)
                            })
                        }
                        week != null -> {
                            val w = Calendar.getInstance()
                            w.add(Calendar.WEEK_OF_MONTH, 1)
                            viewState.orderTransports(it.filter { task ->
                                task.startDate.toDateDay()
                                    .after(Date()) && task.startDate.toDateDay().before(w.time)
                            })
                        }
                        else -> {
                            viewState.orderTransports(it.filter { task -> DateUtils.isToday(task.startDate.toDateDay().time) })
                        }
                    }
                },
                { errorHandler.proceed(it, { viewState.showMessage(it) }) }
            )
            .connect()
    }

}