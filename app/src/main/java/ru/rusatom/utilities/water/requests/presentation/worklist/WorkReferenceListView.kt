package ru.rusatom.utilities.water.requests.presentation.worklist

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.rusatom.utilities.water.requests.entity.WorkList

interface WorkReferenceListView : MvpView{
    fun showProgress(b: Boolean)
    fun setWorks(it: List<WorkList>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(it: String)
}