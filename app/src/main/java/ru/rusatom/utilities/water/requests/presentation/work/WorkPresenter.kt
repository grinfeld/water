package ru.rusatom.utilities.water.requests.presentation.work

import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.entity.OrderTS
import ru.rusatom.utilities.water.requests.entity.Work
import ru.rusatom.utilities.water.requests.model.interactor.OrderTSInteractor
import ru.rusatom.utilities.water.requests.model.interactor.WorksInteractor
import ru.rusatom.utilities.water.requests.model.system.flow.FlowRouter
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import ru.rusatom.utilities.water.requests.presentation.global.ErrorHandler
import javax.inject.Inject

@InjectViewState
class WorkPresenter @Inject constructor(
    private val workInteractor: WorksInteractor,
    private val errorHandler: ErrorHandler,
    private val flowRouter: FlowRouter
) : BasePresenter<WorkView>() {
    fun openAddWork(taskId: Int) {
        flowRouter.startFlow(Screens.WorkReferenceListFlow(taskId))
    }

    fun getWorkById(taskId: Int) {
        workInteractor.getWorkById(taskId)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { viewState.onWorks(it) },
                { throwable -> errorHandler.proceed(throwable) { viewState.showMessage(it) } }
            )
            .connect()
    }

    fun delWork(work: Work) {
        workInteractor.delWork(work)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { viewState.onDelWork() },
                { throwable -> errorHandler.proceed(throwable) { viewState.showMessage(it) } }
            )
            .connect()
    }

    fun editWork(work: Work) {
        workInteractor.editWork(work)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { viewState.onEditWork() },
                { throwable -> errorHandler.proceed(throwable) { viewState.showMessage(it) } }
            )
            .connect()
    }

}