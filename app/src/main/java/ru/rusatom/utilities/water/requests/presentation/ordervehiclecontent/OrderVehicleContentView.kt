package ru.rusatom.utilities.water.requests.presentation.ordervehiclecontent

import com.arellomobile.mvp.MvpView

interface OrderVehicleContentView : MvpView {
    fun showProgress(b: Boolean)
    fun changeStatus(it: Int)
    fun showMessage(it: String)
}