package ru.rusatom.utilities.water.requests.presentation.main

import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.entity.Role
import ru.rusatom.utilities.water.requests.model.data.storage.Prefs
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import javax.inject.Inject

@InjectViewState
class MainPresenter @Inject constructor(
    private var prefs: Prefs
) : BasePresenter<MainView>() {

    fun getRole() {
        val permissions = prefs.accounts?.user?.permissions
        var admin: Boolean
        admin = permissions?.contains(Role.ADMIN.int)!!
        if (!admin) {
            admin = permissions.containsAll(arrayListOf(Role.DRIVER.int, Role.TASKMASTER.int))
        }
        val driver = permissions.contains(Role.DRIVER.int)
        val foreman = permissions.contains(Role.TASKMASTER.int)
        when {
            admin -> viewState.onUserRole(Role.ADMIN.int)
            driver -> viewState.onUserRole(Role.DRIVER.int)
            foreman -> viewState.onUserRole(Role.TASKMASTER.int)
            else -> viewState.onUserRole(Role.UNKNOWN.int)
        }
    }

}