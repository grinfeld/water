package ru.rusatom.utilities.water.requests.ui.filter

import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_filter.*
import kotlinx.android.synthetic.main.fragment_map.toolbar
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.filter.DateFilter
import ru.rusatom.utilities.water.requests.presentation.filter.FilterPresenter
import ru.rusatom.utilities.water.requests.presentation.filter.FilterView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.ui.global.dialog.FilterDateDialog
import ru.rusatom.utilities.water.requests.ui.global.dialog.FilterDateDialog.Companion.ARG_LIST_DATE_FILTER
import ru.rusatom.utilities.water.requests.ui.global.dialog.FilterStatusDialog
import ru.rusatom.utilities.water.requests.utils.addSystemTopPadding

class FilterFragment : BaseFragment(), FilterView {

    override val layoutRes = R.layout.fragment_filter

    @InjectPresenter
    lateinit var presenter: FilterPresenter

    @ProvidePresenter
    fun providePresenter(): FilterPresenter = scope.getInstance(FilterPresenter::class.java)


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.apply {
            addSystemTopPadding()
            toolbar.setNavigationOnClickListener { presenter.onBackPressed() }
            inflateMenu(R.menu.clear_filter_menu)
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_clear -> {
                        val prefs = presenter.getPrefs()
                        val dateFilter = prefs.dateFilter
                        dateFilter.forEachIndexed { index, d ->
                            d.isChecked = index == 1
                        }
                        prefs.dateFilter = dateFilter
                        refreshFilterText()
                    }
                }
                true
            }
            refreshFilterText()
            filter_date.setOnClickListener {
                val dialog = FilterDateDialog.create(presenter.getPrefs().dateFilter)
                dialog.setTargetFragment(this@FilterFragment, DATE_FILTER)
                dialog.show(parentFragmentManager, null)
            }
            filter_status.setOnClickListener {
                FilterStatusDialog().show(requireActivity().supportFragmentManager, null)
            }
        }
    }

    private fun refreshFilterText() {
        filter_date.text = presenter.getPrefs().dateFilter
            .filter { dateFilter -> dateFilter.isChecked }.toString()
            .replace("[", "")
            .replace("]", "")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            DATE_FILTER -> {
                val list = data?.getParcelableArrayListExtra<DateFilter>(ARG_LIST_DATE_FILTER)
                if (list != null) {
                    presenter.getPrefs().dateFilter = list
                    refreshFilterText()
                }
            }
        }
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    companion object {
        const val DATE_FILTER = 1
    }


}