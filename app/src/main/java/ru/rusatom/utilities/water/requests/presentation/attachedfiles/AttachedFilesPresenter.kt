package ru.rusatom.utilities.water.requests.presentation.attachedfiles

import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.model.system.flow.FlowRouter
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import javax.inject.Inject

@InjectViewState
class AttachedFilesPresenter @Inject constructor(
    private val flowRouter: FlowRouter
) : BasePresenter<AttachedFilesView>() {


    fun onBackPressed() {
        flowRouter.exit()
    }

    fun openGallery(typeGallery: Int) {
        flowRouter.startFlow(Screens.GalleryFlow(typeGallery))
    }
}