package ru.rusatom.utilities.water.requests.di.provider

import ru.rusatom.utilities.water.requests.BuildConfig
import ru.rusatom.utilities.water.requests.model.server.Tls12SocketFactory.Companion.enableTls12
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import ru.rusatom.utilities.water.requests.model.data.storage.Prefs
import ru.rusatom.utilities.water.requests.model.server.interceptor.HeaderInterceptor
import ru.rusatom.utilities.water.requests.model.server.interceptor.HostSelectionInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Provider

class OkHttpClientProvider @Inject constructor(private val prefs: Prefs) : Provider<OkHttpClient> {

    override fun get(): OkHttpClient = with(OkHttpClient.Builder()) {
        enableTls12()
        connectTimeout(TIMEOUT, TimeUnit.SECONDS)
        readTimeout(TIMEOUT, TimeUnit.SECONDS)
        addNetworkInterceptor(HeaderInterceptor(prefs))
        addInterceptor(HostSelectionInterceptor(prefs))
        if (BuildConfig.DEBUG) {
            addNetworkInterceptor(
                HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
            )
        }
        build()
    }

    companion object {
        private const val TIMEOUT = 30L
    }
}