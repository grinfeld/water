package ru.rusatom.utilities.water.requests.ui.ordervehicle.content

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.entity.OrderVehicle
import ru.rusatom.utilities.water.requests.ui.global.FlowFragment


class OrderVehicleContentFlow(private val orderVehicle: OrderVehicle) : FlowFragment() {

    override fun getLaunchScreen() = Screens.OrderVehicleContent(orderVehicle)

}