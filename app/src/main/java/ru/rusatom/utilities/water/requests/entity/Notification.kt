package ru.rusatom.utilities.water.requests.entity

data class Notification (val name: String, val date: String)