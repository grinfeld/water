package ru.rusatom.utilities.water.requests.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderVehicle (
    @SerializedName("id")
    val id: Int,
    @SerializedName("demandAddress")
    val address: String,
    @SerializedName("demandStartDt")
    val startDate: String,
    @SerializedName("demandEndDt")
    val endDate: String,
    @SerializedName("transportStateId")
    var status: Int,
    @SerializedName("transportStateName")
    val transportStateName: String
) : Parcelable