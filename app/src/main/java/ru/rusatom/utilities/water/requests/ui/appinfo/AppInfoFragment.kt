package ru.rusatom.utilities.water.requests.ui.appinfo

import android.os.Bundle
import androidx.core.text.HtmlCompat
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_app_info.*
import kotlinx.android.synthetic.main.fragment_map.toolbar
import ru.rusatom.utilities.water.requests.BuildConfig
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.presentation.appinfo.AppInfoPresenter
import ru.rusatom.utilities.water.requests.presentation.appinfo.AppInfoView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.utils.addSystemTopPadding

class AppInfoFragment : BaseFragment(), AppInfoView {
    override val layoutRes = R.layout.fragment_app_info

    @InjectPresenter
    lateinit var presenter: AppInfoPresenter

    @ProvidePresenter
    fun providePresenter(): AppInfoPresenter = scope.getInstance(
        AppInfoPresenter::class.java
    )


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.apply {
            addSystemTopPadding()
            inflateMenu(R.menu.log_out_menu)
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_log_out -> {
                        presenter.logOut()
                    }
                }
                true
            }
        }
        version.text = getString(R.string.version, BuildConfig.VERSION_NAME)
        privacy_policy.text = HtmlCompat.fromHtml(
            getString(R.string.privacy_policy_underline),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        privacy_policy.setOnClickListener {
            presenter.openPrivacyPolicy()
        }
        history.text = HtmlCompat.fromHtml(
            getString(R.string.history_version),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        history.setOnClickListener {
            presenter.openHistory()
        }
        tech_support.setOnClickListener {
            presenter.openTechSupport()
        }
    }

}