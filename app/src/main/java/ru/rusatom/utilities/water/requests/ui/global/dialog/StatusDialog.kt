package ru.rusatom.utilities.water.requests.ui.global.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_status.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.status.Status
import ru.rusatom.utilities.water.requests.utils.argument


class StatusDialog : DialogFragment() {

    private var status: Status by argument(ARG_STATUS)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_status, container)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        list.choiceMode = ListView.CHOICE_MODE_SINGLE
        val adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_list_item_single_choice,
            status.getLabelsStatus(requireContext())
        )
        list.adapter = adapter
        cancel_action.setOnClickListener {
            this.dismiss()
        }
        ok_action.setOnClickListener {

        }
    }

    companion object {
        private const val ARG_STATUS = "arg_status"
        fun create(status: Status) =
            StatusDialog().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_STATUS, status)
                }
            }
    }


}