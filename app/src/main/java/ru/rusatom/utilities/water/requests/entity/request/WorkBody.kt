package ru.rusatom.utilities.water.requests.entity.request

import com.google.gson.annotations.SerializedName

data class WorkBody(@SerializedName("id") val id: Int,
                    @SerializedName("taskId") val taskId: Int,
                    @SerializedName("workNameId") val workNameID: Int,
                    @SerializedName("endDt") var dateEnd: String,
                    @SerializedName("description") val descriptor: String)