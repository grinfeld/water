package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_work.view.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.Work
import ru.rusatom.utilities.water.requests.utils.inflate


typealias OpenDelWork = (Work) -> Unit
typealias EditWork = (Work) -> Unit

class WorksAdapter(
    private val openDelWork: OpenDelWork,
    private val editWork: EditWork
) : RecyclerView.Adapter<WorksAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.context.inflate(R.layout.item_work, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(h: ViewHolder, position: Int) {
        val w = differ.currentList[position]
        h.bindView(w)
    }


    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class ViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {
        fun bindView(w: Work) = with(itemView) {
            txtName.text = w.name
            time_end.text = w.dateEnd
            expanded_layout.visibility = if (w.isExpanded) View.VISIBLE else View.GONE
            del.setOnClickListener {
                openDelWork(w)
            }
            edit.setOnClickListener {
                editWork(w)
            }
            itemView.setOnClickListener {
                val expanded: Boolean = w.isExpanded
                w.isExpanded = !expanded
                notifyDataSetChanged()
            }

        }
    }


    private val diffCallback = object : DiffUtil.ItemCallback<Work>() {
        override fun areItemsTheSame(oldItem: Work, newItem: Work): Boolean {
            return oldItem.id == newItem.id &&
                    oldItem.taskId == newItem.taskId &&
                    oldItem.dateEnd == newItem.dateEnd &&
                    oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: Work, newItem: Work): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    fun submitList(works: List<Work>) = differ.submitList(works)

}