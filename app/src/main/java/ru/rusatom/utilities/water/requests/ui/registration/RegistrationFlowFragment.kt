package ru.rusatom.utilities.water.requests.ui.registration

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.ui.global.FlowFragment

class RegistrationFlowFragment : FlowFragment(){

    override fun getLaunchScreen() = Screens.Registration


}