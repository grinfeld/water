package ru.rusatom.utilities.water.requests.model.server.interceptor

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Response
import ru.rusatom.utilities.water.requests.model.data.storage.Prefs
import javax.inject.Inject


class HostSelectionInterceptor @Inject constructor(private val prefs: Prefs) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val url = HttpUrl.parse(prefs.serverPath)
        val newRequest = url?.let {
            val newUrl = chain.request().url().newBuilder()
                .scheme(it.scheme())
                .host(it.url().toURI().host)
                .port(it.port())
                .build()
            return@let chain.request().newBuilder()
                .url(newUrl)
                .build()
        }
        return chain.proceed(newRequest)
    }
}


