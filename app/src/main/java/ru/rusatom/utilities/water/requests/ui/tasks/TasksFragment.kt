package ru.rusatom.utilities.water.requests.ui.tasks

import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_map.toolbar
import kotlinx.android.synthetic.main.fragment_order_vehicle.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.Task
import ru.rusatom.utilities.water.requests.entity.status.StatusImpl
import ru.rusatom.utilities.water.requests.presentation.tasks.TasksPresenter
import ru.rusatom.utilities.water.requests.presentation.tasks.TasksView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.ui.global.dialog.StatusDialog
import ru.rusatom.utilities.water.requests.utils.addSystemTopPadding

class TasksFragment : BaseFragment(), TasksView, SwipeRefreshLayout.OnRefreshListener {

    override val layoutRes = R.layout.fragment_tasks

    @InjectPresenter
    lateinit var presenter: TasksPresenter

    @ProvidePresenter
    fun providePresenter(): TasksPresenter =
        scope.getInstance(TasksPresenter::class.java)

    private lateinit var tasksAdapter: TasksAdapter


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.apply {
            addSystemTopPadding()
            inflateMenu(R.menu.filter_menu)
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_filter -> {
                        presenter.openFilter()
                    }
                }
                true
            }
        }
        swipeContainer.setOnRefreshListener(this)
        tasksAdapter = TasksAdapter(
            this::openTaskContent
        )
        recycler.layoutManager = LinearLayoutManager(requireContext())
        recycler.adapter = tasksAdapter
        presenter.getTasks()
    }

    private fun openTaskContent(task: Task) {
        presenter.openTaskContent(task)
    }


    override fun showProgress(b: Boolean) {
        swipeContainer.isRefreshing = b
    }

    override fun showMessage(it: String) {
        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
    }

    override fun onTasks(it: List<Task>) {
            tasksAdapter.submitList(it)
    }

    override fun onRefresh() {
        presenter.getTasks()
    }

}