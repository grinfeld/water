package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.works

import android.content.Context
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import ru.rusatom.utilities.water.requests.entity.WorkList

class WorkReferenceAdapter(context: Context, resource: Int, objects: List<WorkList>) :
    ArrayAdapter<WorkList>(context, resource, objects), Filterable{

    override fun getFilter(): Filter {
        return super.getFilter()
    }

}