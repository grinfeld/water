package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.works

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.ui.global.FlowFragment

class WorkReferenceListFlowFragment(val taskId: Int) : FlowFragment() {

    override fun getLaunchScreen() = Screens.WorkList(taskId)

}