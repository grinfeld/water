package ru.rusatom.utilities.water.requests.ui.ordervehicle.content

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_map.toolbar
import kotlinx.android.synthetic.main.fragment_order_vehicle_content.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.OrderVehicle
import ru.rusatom.utilities.water.requests.entity.status.StatusImpl
import ru.rusatom.utilities.water.requests.presentation.ordervehiclecontent.OrderVehicleContentPresenter
import ru.rusatom.utilities.water.requests.presentation.ordervehiclecontent.OrderVehicleContentView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.ui.global.dialog.StatusDialog
import ru.rusatom.utilities.water.requests.utils.*

class OrderVehicleContentFragment : BaseFragment(), OrderVehicleContentView {
    override val layoutRes = R.layout.fragment_order_vehicle_content

    @InjectPresenter
    lateinit var presenter: OrderVehicleContentPresenter

    @ProvidePresenter
    fun providePresenter(): OrderVehicleContentPresenter =
        scope.getInstance(OrderVehicleContentPresenter::class.java)

    private var orderVehicle: OrderVehicle by argument(ARG_ORDER_VEHICLE)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        view?.addSystemBottomPadding()
        toolbar.apply {
            addSystemTopPadding()
            toolbar.setNavigationOnClickListener { presenter.onBackPressed() }
            title = orderVehicle.id.toString()
        }
        address.text = orderVehicle.address
        date_start.text = orderVehicle.startDate
        date_end.text = orderVehicle.endDate
        status.text = orderVehicle.transportStateName
//        status.setOnClickListener {
//            StatusDialog.create(StatusImpl.ORDER_VEHICLE)
//                .show(requireActivity().supportFragmentManager, null)
//        }
        btn_state_plus.setOnClickListener {
            if (orderVehicle.status == 7){
                presenter.changeState(orderVehicle.id, 5)
            }else{
                presenter.changeState(orderVehicle.id, orderVehicle.status + 1)
            }

        }
        btn_state_minus.setOnClickListener {
            presenter.changeState(orderVehicle.id, 7)
        }
        asd(orderVehicle.status)
    }

    private fun asd(stateId: Int) {
        when (stateId) {
            1 -> btn_container.visibility = View.GONE
            2 -> {
                btn_container.visibility = View.VISIBLE
                btn_state_plus.text = getString(R.string.order_ts_status_confirmed_btn)
                btn_state_minus.visibility = View.GONE
            }
            3 -> {
                btn_container.visibility = View.VISIBLE
                btn_state_plus.text = getString(R.string.order_ts_status_way_btn)
                btn_state_minus.visibility = View.GONE
            }
            4 -> {
                btn_container.visibility = View.VISIBLE
                btn_state_minus.visibility = View.GONE
                btn_state_plus.text = getString(R.string.order_ts_status_in_work_btn)
            }
            5 -> {
                btn_container.visibility = View.VISIBLE
                btn_state_plus.text = getString(R.string.order_ts_status_complete_btn)
                btn_state_minus.visibility = View.VISIBLE
                btn_state_minus.text = getString(R.string.order_ts_status_wait_btn)
            }
            6 -> {
                btn_container.visibility = View.GONE
            }
            7 -> {
                btn_container.visibility = View.VISIBLE
                btn_state_plus.text = getString(R.string.order_ts_status_in_work_btn)
                btn_state_minus.visibility = View.GONE
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        presenter.onBackPressed()
    }

    companion object {
        private const val ARG_ORDER_VEHICLE = "arg_order_vehicle"
        fun create(orderVehicle: OrderVehicle) =
            OrderVehicleContentFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_ORDER_VEHICLE, orderVehicle)
                }
            }
    }

    override fun showProgress(b: Boolean) {
        showProgressDialog(b)
    }

    override fun changeStatus(it: Int) {
        asd(it)
        orderVehicle.status = it
        status.text = StatusImpl.ORDER_VEHICLE.text(it-1, requireContext())
    }

    override fun showMessage(it: String) {

    }

}