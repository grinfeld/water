package ru.rusatom.utilities.water.requests.entity

import com.google.gson.annotations.SerializedName

data class State(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("description") val descriptor: String
)
