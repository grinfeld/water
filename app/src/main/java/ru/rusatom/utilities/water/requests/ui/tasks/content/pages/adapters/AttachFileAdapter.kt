package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionManager
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.MediaData


class AttachFileAdapter(
    private val context: Context,
    private val list: List<MediaData>
) : RecyclerView.Adapter<AttachFileAdapter.ViewHolder>() {

    private var expandedPosition = RecyclerView.NO_POSITION

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_media, parent, false)
        )
    }

    override fun onBindViewHolder(h: ViewHolder, position: Int) {
        val mediaData = list[position]
        val isExpanded = position == expandedPosition
        h.detail.visibility = if (isExpanded) View.VISIBLE else View.GONE
        h.itemView.setOnClickListener {
            expandedPosition = if (isExpanded) {
                RecyclerView.NO_POSITION
            } else {
                position
            }
            notifyItemChanged(position)
        }

    }


    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val detail: LinearLayout = view.findViewById(R.id.detail)

    }

}