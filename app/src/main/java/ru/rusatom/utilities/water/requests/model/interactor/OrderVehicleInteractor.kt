package ru.rusatom.utilities.water.requests.model.interactor

import io.reactivex.Single
import ru.rusatom.utilities.water.requests.entity.OrderVehicle
import ru.rusatom.utilities.water.requests.model.data.storage.Prefs
import ru.rusatom.utilities.water.requests.model.server.Api
import ru.rusatom.utilities.water.requests.model.system.SchedulersProvider
import javax.inject.Inject

class OrderVehicleInteractor @Inject constructor(
    private val api: Api,
    private val schedulers: SchedulersProvider,
    private val prefs: Prefs
) {
    fun getOrderTSItems(): Single<List<OrderVehicle>> {
        val identifier = prefs.accounts?.user?.identifier
        return api.getTransportOrderItemsById(identifier!!)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }
}