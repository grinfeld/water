package ru.rusatom.utilities.water.requests.ui.ordervehicle

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_nav_drawer.view.*
import kotlinx.android.synthetic.main.item_order_vehicle.view.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.OrderVehicle
import ru.rusatom.utilities.water.requests.utils.inflate

typealias OpenOrderVehicleContentClickListener = (OrderVehicle) -> Unit

class OrderVehicleAdapter(
    private val openOrderVehicleContentClickListener: OpenOrderVehicleContentClickListener
) : RecyclerView.Adapter<OrderVehicleAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.context.inflate(R.layout.item_order_vehicle, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val orderVehicle = differ.currentList[position]
        holder.bindView(orderVehicle)
    }

    override fun getItemCount(): Int {
        return differ.currentList.count()
    }

    inner class ViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {
        fun bindView(orderVehicle: OrderVehicle) = with(itemView) {
            txtName.text = orderVehicle.id.toString()
            address.text = orderVehicle.address
            date_start.text = orderVehicle.startDate
            date_end.text = orderVehicle.endDate
            status.text = orderVehicle.transportStateName
            itemView.setOnClickListener {
                openOrderVehicleContentClickListener(orderVehicle)
            }
        }
    }


    private val diffCallback = object : DiffUtil.ItemCallback<OrderVehicle>() {
        override fun areItemsTheSame(oldItem: OrderVehicle, newItem: OrderVehicle): Boolean {
            return oldItem.id == newItem.id &&
                    oldItem.address == newItem.address &&
                    oldItem.startDate == newItem.startDate &&
                    oldItem.endDate == newItem.endDate
        }

        override fun areContentsTheSame(oldItem: OrderVehicle, newItem: OrderVehicle): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    fun submitList(orderVehicles: List<OrderVehicle>) = differ.submitList(orderVehicles)

}