package ru.rusatom.utilities.water.requests.presentation.tasks

import android.text.format.DateUtils
import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.entity.Task
import ru.rusatom.utilities.water.requests.model.data.storage.Prefs
import ru.rusatom.utilities.water.requests.model.interactor.TasksInteractor
import ru.rusatom.utilities.water.requests.model.system.flow.FlowRouter
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import ru.rusatom.utilities.water.requests.presentation.global.ErrorHandler
import ru.rusatom.utilities.water.requests.utils.toDateDay
import java.util.*
import javax.inject.Inject

@InjectViewState
class TasksPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val errorHandler: ErrorHandler,
    private val orderVehicleInteractor: TasksInteractor,
    private val prefs: Prefs
) : BasePresenter<TasksView>() {

    fun openTaskContent(task: Task) {
        flowRouter.startFlow(Screens.TaskContentPagesFlow(task))
    }

    fun openFilter() {
        flowRouter.startFlow(Screens.FilterFlow)
    }

    fun getTasks() {
        orderVehicleInteractor.getTasks()
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                {
                    val dateFilter = prefs.dateFilter.filter { dateFilter -> dateFilter.isChecked }
                    val all = dateFilter.firstOrNull() { dF -> dF.type == 0 }
                    val week = dateFilter.firstOrNull() { dF -> dF.type == 2 }
                    val mouth = dateFilter.firstOrNull() { dF -> dF.type == 3 }
                    when {
                        all != null -> {
                            viewState.onTasks(it)
                        }
                        mouth != null -> {
                            val m = Calendar.getInstance()
                            m.add(Calendar.MONTH, 1)
                            viewState.onTasks(it.filter { task ->
                                task.planStartDt.toDateDay()
                                    .after(Date()) && task.planStartDt.toDateDay().before(m.time)
                            })
                        }
                        week != null -> {
                            val w = Calendar.getInstance()
                            w.add(Calendar.WEEK_OF_MONTH, 1)
                            viewState.onTasks(it.filter { task ->
                                task.planStartDt.toDateDay()
                                    .after(Date()) && task.planStartDt.toDateDay().before(w.time)
                            })
                        }
                        else -> {
                            viewState.onTasks(it.filter { task -> DateUtils.isToday(task.planStartDt.toDateDay().time) })
                        }
                    }
                },
                { errorHandler.proceed(it, { viewState.showMessage(it) }) }
            )
            .connect()
    }
}