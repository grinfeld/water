package ru.rusatom.utilities.water.requests.model.data.storage

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ru.rusatom.utilities.water.requests.entity.AuthData
import ru.rusatom.utilities.water.requests.entity.filter.DateFilter
import javax.inject.Inject

class Prefs @Inject constructor(
    private val context: Context,
    private val gson: Gson
) {

    private fun getSharedPreferences(prefsName: String) =
        context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)

    //region auth
    private val AUTH_DATA = "auth_data"
    private val KEY_USER_ACCOUNTS = "accounts"
    private val authPrefs by lazy { getSharedPreferences(AUTH_DATA) }


    private val accountsTypeToken = object : TypeToken<AuthData>() {}.type
    var accounts: AuthData?
        get() {
            return gson.fromJson(authPrefs.getString(KEY_USER_ACCOUNTS, null), accountsTypeToken)
        }
        set(value) {
            authPrefs.edit().putString(KEY_USER_ACCOUNTS, gson.toJson(value)).apply()
        }


    //endregion

    //region app
    private val APP_DATA = "app_data"
    private val KEY_FIRST_LAUNCH_TIME = "launch_ts"
    private val KEY_SERVER = "server"
    private val KEY_DATE_FILTER = "date_filter"
    private val appPrefs by lazy { getSharedPreferences(APP_DATA) }

    var firstLaunchTimeStamp: Long?
        get() = appPrefs.getLong(KEY_FIRST_LAUNCH_TIME, 0).takeIf { it > 0 }
        set(value) {
            appPrefs.edit().putLong(KEY_FIRST_LAUNCH_TIME, value ?: 0).apply()
        }

    var serverPath: String
        get() = appPrefs.getString(KEY_SERVER, "http://example.com")!!
        set(value) {
            appPrefs.edit().putString(KEY_SERVER, value).apply()
        }


    private val dateFilterTypeToken = object : TypeToken<ArrayList<DateFilter>>() {}.type
    var dateFilter: ArrayList<DateFilter>
        get() {
            return gson.fromJson(appPrefs.getString(KEY_DATE_FILTER, "[{\"isChecked\":false,\"name\":" +
                    "\"Все\",\"type\":0},{\"isChecked\":true,\"name\":\"Сегодня\",\"type\":1}," +
                    "{\"isChecked\":false,\"name\":\"На неделю\",\"type\":2},{\"isChecked\":false," +
                    "\"name\":\"На месяц\",\"type\":3}]"), dateFilterTypeToken)
        }
        set(value) {
            appPrefs.edit().putString(KEY_DATE_FILTER, gson.toJson(value)).apply()
        }
    //endregion
}