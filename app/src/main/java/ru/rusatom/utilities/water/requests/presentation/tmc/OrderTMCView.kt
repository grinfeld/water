package ru.rusatom.utilities.water.requests.presentation.tmc

import com.arellomobile.mvp.MvpView
import ru.rusatom.utilities.water.requests.entity.OrderTMC.MaterialOrderItems

interface OrderTMCView : MvpView {
    fun showProgress(b: Boolean)
    fun showMessage(it: String)
    fun orderTMC(it: List<MaterialOrderItems>)
    fun showRefreshProgress(b: Boolean)
    fun onFactContChange()
}