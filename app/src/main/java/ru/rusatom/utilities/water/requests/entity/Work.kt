package ru.rusatom.utilities.water.requests.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Work(
    @SerializedName("id") val id: Int,
    @SerializedName("taskId") val taskId: Int,
    @SerializedName("workNameId") val workNameID: Int,
    @SerializedName("workName") val name: String,
    @SerializedName("endDt") var dateEnd: String,
    @SerializedName("description") val descriptor: String,
    var isExpanded: Boolean = false
) : Parcelable