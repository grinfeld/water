package ru.rusatom.utilities.water.requests.presentation.tscontent

import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.model.interactor.OrderTSContentInteractor
import ru.rusatom.utilities.water.requests.model.system.flow.FlowRouter
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import ru.rusatom.utilities.water.requests.presentation.global.ErrorHandler
import javax.inject.Inject

@InjectViewState
class OrderTSContentPresenter @Inject constructor(
    private val orderTSContentInteractor: OrderTSContentInteractor,
    private val errorHandler: ErrorHandler,
    private val flowRouter: FlowRouter
) : BasePresenter<OrderTSContentView>() {

    fun onBackPressed() {
        flowRouter.exit()
    }

    fun resetArrivalDt(id: Int) {
        orderTSContentInteractor.resetArrivalDt(id)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { viewState.onArrivalDt("") },
                { errorHandler.proceed(it, { viewState.showMessage(it) }) }
            )
            .connect()
    }

    fun setArrivalDt(id: Int) {
        orderTSContentInteractor.setArrivalDt(id)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { viewState.onArrivalDt(it.value) },
                { errorHandler.proceed(it, { viewState.showMessage(it) }) }
            )
            .connect()
    }

    fun resetDepartureDt(id: Int) {
        orderTSContentInteractor.resetDepartureDt(id)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { viewState.onDepartureDt("") },
                { errorHandler.proceed(it, { viewState.showMessage(it) }) }
            )
            .connect()
    }

    fun setDepartureDt(id: Int) {
        orderTSContentInteractor.setDepartureDt(id)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { viewState.onDepartureDt(it.value) },
                { errorHandler.proceed(it, { viewState.showMessage(it) }) }
            )
            .connect()
    }

}