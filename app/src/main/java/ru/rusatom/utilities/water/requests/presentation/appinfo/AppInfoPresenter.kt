package ru.rusatom.utilities.water.requests.presentation.appinfo

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.model.data.storage.Prefs
import ru.rusatom.utilities.water.requests.model.system.flow.FlowRouter
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import javax.inject.Inject

class AppInfoPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val prefs: Prefs
) : BasePresenter<AppInfoView>() {


    fun openPrivacyPolicy() {
        flowRouter.startFlow(Screens.Privacy)
    }

    fun logOut() {
        prefs.accounts = null
        flowRouter.newRootScreen(Screens.RegistrationFlow)
    }

    fun openHistory() {
        flowRouter.startFlow(Screens.History)
    }

    fun openTechSupport() {
        flowRouter.startFlow(Screens.TechSupport)
    }

}