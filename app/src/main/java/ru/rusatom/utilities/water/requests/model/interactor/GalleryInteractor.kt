package ru.rusatom.utilities.water.requests.model.interactor

import android.content.Context
import android.provider.MediaStore
import android.util.Log
import io.reactivex.Observable
import ru.rusatom.utilities.water.requests.entity.MediaData
import ru.rusatom.utilities.water.requests.model.system.SchedulersProvider
import javax.inject.Inject

class GalleryInteractor @Inject constructor(
    private val context: Context,
    private val schedulers: SchedulersProvider
) {

    fun getVideo(): Observable<ArrayList<MediaData>> =
        Observable.create<ArrayList<MediaData>> {
            val result = ArrayList<MediaData>()
            val videoProjection = arrayOf(
                MediaStore.Video.Media._ID,
                MediaStore.Video.Media.DATA,
                MediaStore.Video.Media.DATE_ADDED,
                MediaStore.Video.Media.DURATION
            )
            val videoQueryUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
            val videoCursor =
                context.contentResolver.query(videoQueryUri, videoProjection, null, null, null)

            try {
                if (videoCursor != null && videoCursor.count > 0) {
                    if (videoCursor.moveToFirst()) {
                        val idColumn = videoCursor.getColumnIndex(MediaStore.Video.Media._ID)
                        val dataColumn = videoCursor.getColumnIndex(MediaStore.Video.Media.DATA)
                        val dateAddedColumn =
                            videoCursor.getColumnIndex(MediaStore.Video.Media.DATE_ADDED)
                        val durationColumn =
                            videoCursor.getColumnIndex(MediaStore.Video.Media.DURATION)
                        do {
                            val id = videoCursor.getString(idColumn)
                            val data = videoCursor.getString(dataColumn)
                            val dateAdded = videoCursor.getString(dateAddedColumn)
                            val duration = videoCursor.getInt(durationColumn)
                            val galleryData = MediaData()
                            galleryData.url = data
                            galleryData.id = Integer.valueOf(id)
                            galleryData.duration = duration
                            galleryData.mediaType = MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO
                            galleryData.dateAdded = dateAdded
                            result.add(galleryData)
                        } while (videoCursor.moveToNext())
                    }
                    videoCursor.close()
                }
            } catch (e: Exception) {
                it.onError(e)
            } finally {
                it.onNext(result)
                it.onComplete()
            }
        }.subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun getPhotos(): Observable<ArrayList<MediaData>> =
        Observable.create<ArrayList<MediaData>> {
            val result = ArrayList<MediaData>()
            val imagesProjection = arrayOf(
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.DATE_ADDED
            )
            val imagesQueryUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            val imagesCursor = context.contentResolver.query(
                imagesQueryUri, imagesProjection,
                null, null, null
            )

            try {
                if (imagesCursor != null && imagesCursor.count > 0) {
                    val count = imagesCursor.count
                    Log.d("ASDASDasdsa", "imagesCursor.count $count ")
                    if (imagesCursor.moveToFirst()) {
                        val idColumn = imagesCursor.getColumnIndex(MediaStore.Images.Media._ID)
                        val dataColumn = imagesCursor.getColumnIndex(MediaStore.Images.Media.DATA)
                        val dateAddedColumn =
                            imagesCursor.getColumnIndex(MediaStore.Images.Media.DATE_ADDED)
                        do {
                            val id = imagesCursor.getString(idColumn)
                            val data = imagesCursor.getString(dataColumn)
                            val dateAdded = imagesCursor.getString(dateAddedColumn)
                            val galleryData = MediaData()
                            galleryData.url = data
                            galleryData.id = Integer.valueOf(id)
                            galleryData.mediaType = MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
                            galleryData.dateAdded = dateAdded
                            result.add(galleryData)
                        } while (imagesCursor.moveToNext())
                    }
                    imagesCursor.close()
                }
            } catch (e: Exception) {
                it.onError(e)
            } finally {
                it.onNext(result)
                it.onComplete()
            }
        }.subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())


}