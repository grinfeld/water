package ru.rusatom.utilities.water.requests.presentation.work

import com.arellomobile.mvp.MvpView
import ru.rusatom.utilities.water.requests.entity.Work

interface WorkView : MvpView {
    fun showProgress(b: Boolean)
    fun showMessage(it: String)
    fun onWorks(it: List<Work>)
    fun onDelWork()
    fun onEditWork()
}