package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.tmc

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.ui.global.FlowFragment

class OrderTMCFlowFragment(private val taskId: Int) : FlowFragment() {

    override fun getLaunchScreen() = Screens.OrderTMC(taskId)

}