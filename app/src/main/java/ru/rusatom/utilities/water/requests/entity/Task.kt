package ru.rusatom.utilities.water.requests.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Task(
    @SerializedName("id") val id: Int,
    @SerializedName("taskTypeName") val taskTypeName: String,
    @SerializedName("requestId") val requestId: Int,
    @SerializedName("planStartDt") val planStartDt: String,
    @SerializedName("planEndDt") val planEndDt: String,
    @SerializedName("taskStateName") val taskStateName: String,
    @SerializedName("taskStateId") var taskStateId: Int,
    @SerializedName("devices") val devices: List<Devices>
    //@SerializedName("materialOrderItems") val materialOrderItems : List<MaterialOrderItems>,
    // @SerializedName("transportOrderItems") val transportOrderItems : List<TransportOrderItems>,
    // @SerializedName("works") val works : List<String>
) : Parcelable {
    @Parcelize
    data class Devices(
        @SerializedName("name") val name: String,
        @SerializedName("address") val address: String
    ) : Parcelable
}


