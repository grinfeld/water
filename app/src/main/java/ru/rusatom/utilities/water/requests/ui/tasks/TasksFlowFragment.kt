package ru.rusatom.utilities.water.requests.ui.tasks

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.ui.global.FlowFragment

class TasksFlowFragment : FlowFragment(){

    override fun getLaunchScreen() = Screens.Tasks

}