package ru.rusatom.utilities.water.requests.ui.privacypolicy

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import androidx.core.text.HtmlCompat
import kotlinx.android.synthetic.main.fragment_policy.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.di.DI
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.utils.addSystemBottomPadding
import ru.rusatom.utilities.water.requests.utils.addSystemTopPadding
import ru.terrakok.cicerone.Router
import toothpick.Toothpick
import javax.inject.Inject

class PrivacyPolicyFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_policy

    override val parentScopeName = DI.APP_SCOPE

    @Inject
    lateinit var router: Router

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Toothpick.inject(this, scope)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.addSystemTopPadding()
        view.addSystemBottomPadding()
        okButton.setOnClickListener { onBackPressed() }
        val html = getString(R.string.privacy_policy_text)
        val styledText = HtmlCompat.fromHtml(html, HtmlCompat.FROM_HTML_MODE_LEGACY)
        tvContent.text = styledText
        tvContent.isClickable = true
        tvContent.movementMethod = LinkMovementMethod.getInstance()
    }


    override fun onBackPressed() {
        super.onBackPressed()
        router.exit()
    }

}