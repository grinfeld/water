package ru.rusatom.utilities.water.requests.entity

import com.google.gson.annotations.SerializedName

data class OrderTSApprovedDt(@SerializedName("value") val value: String)