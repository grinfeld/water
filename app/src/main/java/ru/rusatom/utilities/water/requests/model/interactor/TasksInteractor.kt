package ru.rusatom.utilities.water.requests.model.interactor

import io.reactivex.Single
import ru.rusatom.utilities.water.requests.entity.Task
import ru.rusatom.utilities.water.requests.model.data.storage.Prefs
import ru.rusatom.utilities.water.requests.model.server.Api
import ru.rusatom.utilities.water.requests.model.system.SchedulersProvider
import javax.inject.Inject

class TasksInteractor@Inject constructor(
    private val api: Api,
    private val schedulers: SchedulersProvider,
    private val prefs: Prefs
) {
    fun getTasks(): Single<List<Task>> {
        val identifier = prefs.accounts?.user?.identifier
        return api.getTaskById(identifier!!)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

}