package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_order_tmc.view.*
import kotlinx.android.synthetic.main.item_task.view.txtName
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.OrderTMC
import ru.rusatom.utilities.water.requests.entity.OrderTMC.MaterialOrderItems
import ru.rusatom.utilities.water.requests.entity.Task
import ru.rusatom.utilities.water.requests.utils.*

typealias OpenEditFactCountOrderTMC = (MaterialOrderItems) -> Unit

class OrderTMCAdapter(
    private val openEditFactCountOrderTMC: OpenEditFactCountOrderTMC
) : RecyclerView.Adapter<OrderTMCAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.context.inflate(R.layout.item_order_tmc, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(h: ViewHolder, position: Int) {
        val materialOrderItem = differ.currentList[position]
        h.bindView(materialOrderItem)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class ViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {
        fun bindView(m: MaterialOrderItems) =
            with(itemView) {
                txtName.text = m.materialName
                status.text = m.stateName
                // related_order_tmc.text = m.related_order
                measure.text = m.material.unit.shortName
                plan_count.text = m.planQuantity.format()
                fact_count.text = m.factQuantity.format()
                edit_fact_count.setOnClickListener {
                    openEditFactCountOrderTMC(m)
                }
            }
    }


    private val diffCallback = object : DiffUtil.ItemCallback<MaterialOrderItems>() {
        override fun areItemsTheSame(oldItem: MaterialOrderItems, newItem: MaterialOrderItems): Boolean {
            return oldItem.id == newItem.id &&
                    oldItem.materialId == newItem.materialId &&
                    oldItem.materialName == newItem.materialName &&
                    oldItem.planQuantity == newItem.planQuantity &&
                    oldItem.factQuantity == newItem.factQuantity
        }

        override fun areContentsTheSame(oldItem: MaterialOrderItems, newItem: MaterialOrderItems): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    fun submitList(m: List<MaterialOrderItems>) = differ.submitList(m)


}