package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.adapters

import android.content.Context
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.MediaData
import ru.rusatom.utilities.water.requests.utils.DateUtil


class GalleryAdapter(
    private val context: Context,
    private val mediaList: ArrayList<MediaData>
) : RecyclerView.Adapter<GalleryAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_gallery, parent, false)
        )
    }

    override fun onBindViewHolder(h: ViewHolder, position: Int) {
        val m = mediaList[position]
        Glide.with(context)
            .load(m.url)
            .into(h.image)
        if (m.mediaType == MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO) {
            h.durationFrame.visibility = View.VISIBLE
            h.durationLabel.text = DateUtil().millisToTime(m.duration.toLong())
        }
        h.itemView.setOnClickListener {
            m.isSelected = !m.isSelected
            if (m.isSelected){
                h.checkbox.background = ContextCompat.getDrawable(context, R.mipmap.tick)
            }else{
                h.checkbox.background = ContextCompat.getDrawable(context, R.drawable.ic_round)
            }
        }
    }

    override fun getItemCount(): Int {
        return mediaList.size
    }

    public fun getCheckItems(): List<MediaData> {
        return mediaList.filter { mediaData -> mediaData.isSelected }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.findViewById(R.id.image)
        val durationFrame: FrameLayout = view.findViewById(R.id.durationFrame)
        val durationLabel: TextView = view.findViewById(R.id.durationLabel)
        val checkbox: ImageView = view.findViewById(R.id.checkbox)
    }


}