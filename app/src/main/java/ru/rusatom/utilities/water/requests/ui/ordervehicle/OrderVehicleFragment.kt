package ru.rusatom.utilities.water.requests.ui.ordervehicle

import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_map.toolbar
import kotlinx.android.synthetic.main.fragment_order_vehicle.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.OrderVehicle
import ru.rusatom.utilities.water.requests.entity.status.StatusImpl
import ru.rusatom.utilities.water.requests.presentation.ordervehicle.OrderVehiclePresenter
import ru.rusatom.utilities.water.requests.presentation.ordervehicle.OrderVehicleView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.ui.global.dialog.StatusDialog
import ru.rusatom.utilities.water.requests.utils.addSystemTopPadding


class OrderVehicleFragment : BaseFragment(), OrderVehicleView,
    SwipeRefreshLayout.OnRefreshListener {

    override val layoutRes = R.layout.fragment_order_vehicle

    @InjectPresenter
    lateinit var presenter: OrderVehiclePresenter

    @ProvidePresenter
    fun providePresenter(): OrderVehiclePresenter =
        scope.getInstance(OrderVehiclePresenter::class.java)

    private lateinit var orderVehicleAdapter: OrderVehicleAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.apply {
            addSystemTopPadding()
            inflateMenu(R.menu.filter_menu)
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_filter -> {
                        presenter.openFilter()
                    }
                }
                true
            }
        }
        swipeContainer.setOnRefreshListener(this)
        orderVehicleAdapter =
            OrderVehicleAdapter(
                this::openOrderVehicleContent
            )
        recycler.layoutManager = LinearLayoutManager(requireContext())
        recycler.adapter = orderVehicleAdapter
        presenter.getOrderTransports()
    }

    private fun openOrderVehicleContent(orderVehicle: OrderVehicle) {
        presenter.openOrderVehicleContent(orderVehicle)
    }


    override fun showProgress(b: Boolean) {
        swipeContainer.isRefreshing = b
    }

    override fun orderTransports(it: List<OrderVehicle>?) {
        it?.let { it1 -> orderVehicleAdapter.submitList(it1) }
    }

    override fun showMessage(it: String) {
        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
    }

    override fun onRefresh() {
        presenter.getOrderTransports()
    }

}