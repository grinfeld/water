package ru.rusatom.utilities.water.requests.ui.main

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter
import kotlinx.android.synthetic.main.fragment_main.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.entity.Role
import ru.rusatom.utilities.water.requests.presentation.main.MainPresenter
import ru.rusatom.utilities.water.requests.presentation.main.MainView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.utils.addSystemBottomPadding
import ru.rusatom.utilities.water.requests.utils.color
import ru.terrakok.cicerone.android.support.SupportAppScreen

class MainFragment : BaseFragment(), MainView {
    override val layoutRes = R.layout.fragment_main

    private val currentTabFragment: BaseFragment?
        get() = childFragmentManager.fragments.firstOrNull { !it.isHidden } as? BaseFragment

    @InjectPresenter
    lateinit var presenter: MainPresenter

    @ProvidePresenter
    fun providePresenter(): MainPresenter = scope.getInstance(MainPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        linearLayout.addSystemBottomPadding(bottomBar, true)
        presenter.getRole()
    }


    private fun selectTab(tab: SupportAppScreen) {
        val currentFragment = currentTabFragment
        val newFragment = childFragmentManager.findFragmentByTag(tab.screenKey)

        if (currentFragment != null && newFragment != null && currentFragment == newFragment) return

        childFragmentManager.beginTransaction().apply {
            if (newFragment == null) add(
                R.id.mainScreenContainer,
                createTabFragment(tab),
                tab.screenKey
            )

            currentFragment?.let {
                hide(it)
                it.userVisibleHint = false
            }
            newFragment?.let {
                show(it)
                it.userVisibleHint = true
            }
        }.commitNow()
    }

    private fun createTabFragment(tab: SupportAppScreen) = tab.fragment

    override fun onBackPressed() {
        currentTabFragment?.onBackPressed()
    }


    override fun onUserRole(role: Int) {
        initMenu(role)
    }

    private fun getBottomMenu(role: Int): Int {
        return when (role) {
            Role.ADMIN.int -> R.menu.admin_botom_menu
            Role.DRIVER.int -> R.menu.driver_bottom_menu
            Role.TASKMASTER.int -> R.menu.taskmaster_bottom_menu
            else -> R.menu.unkown_bottom_menu
        }
    }

    private fun initMenu(role: Int) {
        AHBottomNavigationAdapter(activity, getBottomMenu(role)).apply {
            setupWithBottomNavigation(bottomBar)
            bottomBar.titleState = AHBottomNavigation.TitleState.ALWAYS_SHOW
        }
        with(bottomBar) {
            accentColor = context.color(R.color.colorPrimary)
            inactiveColor = context.color(R.color.black_20)

            setOnTabSelectedListener { position, wasSelected ->
                if (!wasSelected) selectTab(
                    when (position) {
                        0 -> {
                            when (role) {
                                Role.UNKNOWN.int -> {
                                    appInfoTab
                                }
                                Role.TASKMASTER.int -> tasksTab
                                else -> orderVehicleTab
                            }
                        }
                        1 -> if (role == Role.ADMIN.int) tasksTab else mapTab
                        2 -> if (role == Role.ADMIN.int) mapTab else notificationTab
                        3 -> if (role == Role.ADMIN.int) notificationTab else appInfoTab
                        4 -> appInfoTab
                        else -> when {
                            Role.UNKNOWN.int == role -> {
                                appInfoTab
                            }
                            role == Role.TASKMASTER.int -> tasksTab
                            else -> orderVehicleTab
                        }
                    }
                )
                true
            }
        }

        selectTab(
            when (currentTabFragment?.tag) {
                orderVehicleTab.screenKey -> orderVehicleTab
                tasksTab.screenKey -> tasksTab
                mapTab.screenKey -> mapTab
                notificationTab.screenKey -> notificationTab
                appInfoTab.screenKey -> appInfoTab
                else -> when {
                    Role.UNKNOWN.int == role -> {
                        appInfoTab
                    }
                    role == Role.TASKMASTER.int -> tasksTab
                    else -> orderVehicleTab
                }
            }
        )
    }


    companion object {
        private val tasksTab = Screens.TasksFlow
        private val mapTab = Screens.Map
        private val notificationTab = Screens.Notification
        private val appInfoTab = Screens.AppInfo
        private val orderVehicleTab = Screens.OrderVehicleFlow
    }

}