package ru.rusatom.utilities.water.requests.ui.global.dialog

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager.LayoutParams
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_edit_order_tmc_fact_count.*
import kotlinx.android.synthetic.main.fragment_registration.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.OrderTMC.MaterialOrderItems
import ru.rusatom.utilities.water.requests.utils.argument
import ru.rusatom.utilities.water.requests.utils.format
import java.lang.NumberFormatException

class EditFactCountDialog : DialogFragment(), TextView.OnEditorActionListener {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_edit_order_tmc_fact_count, container)
    }

    private val materialOrderItems: MaterialOrderItems by argument(ARG_MATERIAL_ITEM)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog?.window?.setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        fact_count.setText(materialOrderItems.factQuantity.format())
        fact_count.setSelection(fact_count.text.length)
        fact_count.setOnEditorActionListener(this)
        unit_txt.text = materialOrderItems.material.unit.shortName
        cancel_action.setOnClickListener {
            this.dismiss()
        }
        ok_action.setOnClickListener {
            onOkBtn()
        }

    }

    private fun onOkBtn() {
        if (fact_count.text.isNotEmpty()) {
            try {
                val intent = Intent()
                materialOrderItems.factQuantity = fact_count.text.toString().toFloat()
                intent.putExtra(ARG_MATERIAL_ITEM, materialOrderItems)
                targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
                dismiss()
            } catch (e: NumberFormatException) {
                fact_count.error = getString(R.string.error_format)
            }
        } else {
            fact_count.error = getString(R.string.empty_field)
        }
    }


    companion object {
        const val ARG_MATERIAL_ITEM = "material_item"
        fun create(m: MaterialOrderItems) =
            EditFactCountDialog().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_MATERIAL_ITEM, m)
                }
            }
    }

    override fun onEditorAction(p0: TextView?, p1: Int, p2: KeyEvent?): Boolean {
        if (p1 == EditorInfo.IME_ACTION_DONE) {
            onOkBtn()
            return true
        }
        return false
    }
}