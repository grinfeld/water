package ru.rusatom.utilities.water.requests.ui.global.dialog

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_del_confirmation.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.Work
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.utils.argument

class ConfirmationDelDialog : DialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_del_confirmation, container)
    }

    private val work: Work by argument(ARG_WORK_PARC)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        content.text = getString(R.string.del_work, work.name)
        cancel_action.setOnClickListener {
            dismiss()
        }
        ok_action.setOnClickListener {
            val intent = Intent()
            intent.putExtra(ARG_WORK_PARC, work)
            targetFragment?.onActivityResult(targetRequestCode, RESULT_OK, intent)
            dismiss()
        }
    }

    companion object {
        const val ARG_WORK_PARC = "arg_work"
        fun create(work: Work) =
            ConfirmationDelDialog().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_WORK_PARC, work)
                }
            }
    }

}