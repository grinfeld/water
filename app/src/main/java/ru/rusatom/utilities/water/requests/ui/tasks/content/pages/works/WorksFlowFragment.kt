package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.works

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.ui.global.FlowFragment

class WorksFlowFragment(val taskId: Int) : FlowFragment() {

    override fun getLaunchScreen() = Screens.Works(taskId)

}