package ru.rusatom.utilities.water.requests.presentation

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.model.interactor.LaunchInteractor
import ru.terrakok.cicerone.Router
import javax.inject.Inject


class AppLauncher @Inject constructor(
    private val launchInteractor: LaunchInteractor,
    private val router: Router
) {

    fun onLaunch() {
        launchInteractor.signInToLastSession()
    }

    fun coldStart() {
        val rootScreen = if (launchInteractor.hasAccount) Screens.MainFlow else Screens.RegistrationFlow
        if (launchInteractor.isFirstLaunch) {
            router.newRootChain(rootScreen, Screens.Privacy)
        } else {
            router.newRootScreen(rootScreen)
        }
    }
}