package ru.rusatom.utilities.water.requests.presentation.ordervehiclecontent

import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.model.interactor.OrderVehicleContentInteractor
import ru.rusatom.utilities.water.requests.model.system.flow.FlowRouter
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import ru.rusatom.utilities.water.requests.presentation.global.ErrorHandler
import javax.inject.Inject

@InjectViewState
class OrderVehicleContentPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val orderVehicleContentInteractor: OrderVehicleContentInteractor,
    private val errorHandler: ErrorHandler
) : BasePresenter<OrderVehicleContentView>() {

    fun onBackPressed() = flowRouter.exit()

    fun changeState(orderId: Int, status: Int) {
        orderVehicleContentInteractor.changeStatus(orderId, status)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                {
                    viewState.changeStatus(it)
                },
                { throwable -> errorHandler.proceed(throwable) { viewState.showMessage(it) } }
            )
            .connect()
    }

    fun getStateName(stateId: Int){

    }

}