package ru.rusatom.utilities.water.requests.entity

import com.google.gson.annotations.SerializedName

data class AuthData(
    @SerializedName("internalToken") val internalToken: String,
    @SerializedName("expires") val expires: String,
    @SerializedName("user") val user: User,
    var server: String = "unknown"
) {

    data class User(
        @SerializedName("identifier") val identifier: Int,
        @SerializedName("name") val name: String,
        @SerializedName("permissions") var permissions: List<Int>
    )
}