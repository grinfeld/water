package ru.rusatom.utilities.water.requests.entity

enum class Role(val int: Int) {
    ADMIN(9),
    DRIVER(5),
    TASKMASTER(3),
    UNKNOWN(-1)
}