package ru.rusatom.utilities.water.requests.ui.notification

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_notification.view.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.Notification
import ru.rusatom.utilities.water.requests.utils.inflate


class NotificationAdapter() : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.context.inflate(R.layout.item_notification, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val notification = differ.currentList[position]
        holder.bindView(notification)
    }

    override fun getItemCount(): Int {
        return differ.currentList.count()
    }

    class ViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {
        fun bindView(notification: Notification) = with(itemView) {
            txtName.text = notification.name
            txtDate.text = notification.date
        }
    }


    private val diffCallback = object : DiffUtil.ItemCallback<Notification>() {
        override fun areItemsTheSame(oldItem: Notification, newItem: Notification): Boolean {
            return oldItem.name == newItem.name &&
                    oldItem.date == newItem.date
        }

        override fun areContentsTheSame(oldItem: Notification, newItem: Notification): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    fun submitList(notification: List<Notification>) = differ.submitList(notification)

}