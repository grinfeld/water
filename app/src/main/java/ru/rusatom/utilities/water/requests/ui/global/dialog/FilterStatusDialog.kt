package ru.rusatom.utilities.water.requests.ui.global.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_filter_status.*
import ru.rusatom.utilities.water.requests.R

class FilterStatusDialog : DialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_filter_status, container)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        cancel_action.setOnClickListener {
            this.dismiss()
        }
        ok_action.setOnClickListener {

        }
    }

}