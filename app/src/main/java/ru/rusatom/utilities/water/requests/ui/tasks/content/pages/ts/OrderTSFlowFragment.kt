package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.ts

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.ui.global.FlowFragment

class OrderTSFlowFragment(private val taskId: Int) : FlowFragment() {

    override fun getLaunchScreen() = Screens.OrderTS(taskId)

}