package ru.rusatom.utilities.water.requests.presentation.gallery

import com.arellomobile.mvp.MvpView
import ru.rusatom.utilities.water.requests.entity.MediaData
import java.util.ArrayList

interface GalleryView : MvpView{
    fun setPhotos(it: ArrayList<MediaData>)
    fun showMessage(it: Throwable?)
}