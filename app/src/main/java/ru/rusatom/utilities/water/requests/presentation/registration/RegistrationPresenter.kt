package ru.rusatom.utilities.water.requests.presentation.registration

import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.di.DI
import ru.rusatom.utilities.water.requests.di.ServerPath
import ru.rusatom.utilities.water.requests.di.module.ServerModule
import ru.rusatom.utilities.water.requests.model.data.storage.Prefs
import ru.rusatom.utilities.water.requests.model.interactor.AuthInteractor
import ru.rusatom.utilities.water.requests.model.server.ServerError
import ru.rusatom.utilities.water.requests.model.system.flow.FlowRouter
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import ru.rusatom.utilities.water.requests.presentation.global.ErrorHandler
import ru.rusatom.utilities.water.requests.utils.userMessage
import toothpick.Toothpick
import javax.inject.Inject


@InjectViewState
class RegistrationPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val authInteractor: AuthInteractor,
    private val errorHandler: ErrorHandler,
    private val prefs: Prefs
) : BasePresenter<RegistrationView>() {

    fun auth(login: String, password: String, serverPath: String) {
        if (login.isEmpty() || password.isEmpty()) {
            viewState.showMessage("Введите логин и пароль")
        } else {
            prefs.serverPath = serverPath
            authInteractor.auth(login, password)
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe(
                    {
                        prefs.accounts = it
                        getUserInfo(it.user.identifier)
                    },
                    { throwable ->
                        when (throwable) {
                            is ServerError -> when (throwable.errorCode) {
                                401 -> viewState.showMessage("Неправильный логин или пароль")
                            }
                            else -> errorHandler.proceed(throwable, { viewState.showMessage(it) })
                        }
                    }
                )
                .connect()
        }
    }


    private fun getUserInfo(userId: Int) {
        authInteractor.getUserInfo(userId)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                {
                    val accounts = prefs.accounts
                    accounts?.user?.permissions = it.roles
                    prefs.accounts = accounts
                    flowRouter.newRootFlow(Screens.MainFlow)
                },
                { errorHandler.proceed(it, { viewState.showMessage(it) }) }
            )
            .connect()
    }

    fun getServer() {
        viewState.onServer(prefs.serverPath)
    }

}
