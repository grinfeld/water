package ru.rusatom.utilities.water.requests.model.interactor

import ru.rusatom.utilities.water.requests.model.server.Api
import ru.rusatom.utilities.water.requests.model.system.SchedulersProvider
import javax.inject.Inject

class OrderTSContentInteractor @Inject constructor(
    private val api: Api,
    private val schedulers: SchedulersProvider
) {

    fun resetArrivalDt(id: Int) =
        api.resetArrivalDt(id)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun setArrivalDt(id: Int) =
        api.setArrivalDt(id)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun resetDepartureDt(id: Int) =
        api.resetDepartureDt(id)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun setDepartureDt(id: Int) =
        api.setDepartureDt(id)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

}