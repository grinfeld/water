package ru.rusatom.utilities.water.requests.utils

import android.content.Context
import android.widget.TextView
import androidx.core.content.ContextCompat
import ru.rusatom.utilities.water.requests.entity.StatusOrderVehicleTS
import ru.rusatom.utilities.water.requests.entity.StatusTask
import ru.rusatom.utilities.water.requests.entity.StatusTaskOrderTMC

fun TextView.statusOrderVehicleTS(context: Context, status: Int, isDrawableBackground: Boolean) {
    when (status) {
        StatusOrderVehicleTS.APPOINTED.status -> {
            this.text = context.getString(StatusOrderVehicleTS.APPOINTED.strName)
            if (isDrawableBackground) this.background =
                ContextCompat.getDrawable(context, StatusOrderVehicleTS.APPOINTED.drawableRes)
        }
        StatusOrderVehicleTS.CONFIRMED.status -> {
            this.text = context.getString(StatusOrderVehicleTS.CONFIRMED.strName)
            if (isDrawableBackground) this.background =
                ContextCompat.getDrawable(context, StatusOrderVehicleTS.CONFIRMED.drawableRes)
        }
        StatusOrderVehicleTS.WAY.status -> {
            this.text = context.getString(StatusOrderVehicleTS.WAY.strName)
            if (isDrawableBackground) this.background =
                ContextCompat.getDrawable(context, StatusOrderVehicleTS.WAY.drawableRes)
        }
        StatusOrderVehicleTS.WORK.status -> {
            this.text = context.getString(StatusOrderVehicleTS.WORK.strName)
            if (isDrawableBackground) this.background =
                ContextCompat.getDrawable(context, StatusOrderVehicleTS.WORK.drawableRes)
        }
        StatusOrderVehicleTS.COMPLETE.status -> {
            this.text = context.getString(StatusOrderVehicleTS.COMPLETE.strName)
            if (isDrawableBackground) this.background =
                ContextCompat.getDrawable(context, StatusOrderVehicleTS.COMPLETE.drawableRes)
        }
    }
}

fun TextView.statusTaskOrderTMC(context: Context, status: Int) {
    when (status) {
        StatusTaskOrderTMC.NEW.status -> {
            this.text = context.getString(StatusTaskOrderTMC.NEW.strName)
            this.background = ContextCompat.getDrawable(context, StatusTaskOrderTMC.NEW.drawableRes)
        }
        StatusTaskOrderTMC.PROCESSING.status -> {
            this.text = context.getString(StatusTaskOrderTMC.PROCESSING.strName)
            this.background =
                ContextCompat.getDrawable(context, StatusTaskOrderTMC.PROCESSING.drawableRes)
        }
        StatusTaskOrderTMC.REJECTED.status -> {
            this.text = context.getString(StatusTaskOrderTMC.REJECTED.strName)
            this.background =
                ContextCompat.getDrawable(context, StatusTaskOrderTMC.REJECTED.drawableRes)
        }
        StatusTaskOrderTMC.PROVIDED.status -> {
            this.text = context.getString(StatusTaskOrderTMC.PROVIDED.strName)
            this.background =
                ContextCompat.getDrawable(context, StatusTaskOrderTMC.PROVIDED.drawableRes)
        }
    }
}

fun TextView.statusTask(context: Context, status: Int, isDrawableBackground: Boolean) {
    when (status) {
        StatusTask.NEW.status -> {
            this.text = context.getString(StatusTask.NEW.strName)
            if (isDrawableBackground) this.background =
                ContextCompat.getDrawable(context, StatusTask.NEW.drawableRes)
        }
        StatusTask.APPOINTED.status -> {
            this.text = context.getString(StatusTask.APPOINTED.strName)
            if (isDrawableBackground) this.background =
                ContextCompat.getDrawable(context, StatusTask.APPOINTED.drawableRes)
        }
        StatusTask.CONFIRMED.status -> {
            this.text = context.getString(StatusTask.CONFIRMED.strName)
            if (isDrawableBackground) this.background =
                ContextCompat.getDrawable(context, StatusTask.CONFIRMED.drawableRes)
        }
        StatusTask.WORK.status -> {
            this.text = context.getString(StatusTask.WORK.strName)
            if (isDrawableBackground) this.background =
                ContextCompat.getDrawable(context, StatusTask.WORK.drawableRes)
        }
        StatusTask.WAIT.status -> {
            this.text = context.getString(StatusTask.WAIT.strName)
            if (isDrawableBackground) this.background =
                ContextCompat.getDrawable(context, StatusTask.WAIT.drawableRes)
        }
        StatusTask.COMPLETE.status -> {
            this.text = context.getString(StatusTask.COMPLETE.strName)
            if (isDrawableBackground) this.background =
                ContextCompat.getDrawable(context, StatusTask.COMPLETE.drawableRes)
        }

    }
}
