package ru.rusatom.utilities.water.requests.entity.status

import android.content.Context
import android.graphics.drawable.Drawable
import java.io.Serializable

interface Status : Serializable {
    fun text(status: Int, context: Context): String
    fun drawable(status: Int, context: Context): Drawable
    fun getLabelsStatus(context: Context): ArrayList<String>
}

