package ru.rusatom.utilities.water.requests.di.module

import ru.rusatom.utilities.water.requests.di.ServerPath
import ru.rusatom.utilities.water.requests.di.WithErrorHandler
import ru.rusatom.utilities.water.requests.model.server.Api
import okhttp3.OkHttpClient
import ru.rusatom.utilities.water.requests.di.provider.ApiProvider
import ru.rusatom.utilities.water.requests.di.provider.OkHttpClientProvider
import ru.rusatom.utilities.water.requests.di.provider.OkHttpClientWithErrorHandlerProvider
import toothpick.config.Module

class ServerModule(serverPath: String?) : Module() {
    init {
        
        bind(String::class.java).withName(ServerPath::class.java).toInstance(serverPath)

        // Network
        bind(OkHttpClient::class.java).toProvider(OkHttpClientProvider::class.java)
            .providesSingleton()
        bind(OkHttpClient::class.java).withName(WithErrorHandler::class.java)
            .toProvider(OkHttpClientWithErrorHandlerProvider::class.java)
            .providesSingleton()
        bind(Api::class.java).toProvider(ApiProvider::class.java).providesSingleton()

    }
}