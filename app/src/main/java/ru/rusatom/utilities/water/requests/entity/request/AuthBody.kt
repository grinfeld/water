package ru.rusatom.utilities.water.requests.entity.request

data class AuthBody(val login: String, val password: String)