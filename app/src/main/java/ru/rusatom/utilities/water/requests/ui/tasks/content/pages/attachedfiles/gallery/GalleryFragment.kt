package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.attachedfiles.gallery

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.yandex.runtime.Runtime.getApplicationContext
import kotlinx.android.synthetic.main.fragment_gallery.*
import kotlinx.android.synthetic.main.fragment_map.toolbar
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.MediaData
import ru.rusatom.utilities.water.requests.presentation.gallery.GalleryPresenter
import ru.rusatom.utilities.water.requests.presentation.gallery.GalleryView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.adapters.GalleryAdapter
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.attachedfiles.AttachedFilesFragment
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.attachedfiles.SharedViewModel
import ru.rusatom.utilities.water.requests.utils.addSystemBottomPadding
import ru.rusatom.utilities.water.requests.utils.addSystemTopPadding
import ru.rusatom.utilities.water.requests.utils.argument
import java.io.File


class GalleryFragment : BaseFragment(), GalleryView {

    override val layoutRes = R.layout.fragment_gallery


    @InjectPresenter
    lateinit var presenter: GalleryPresenter

    @ProvidePresenter
    fun providePresenter(): GalleryPresenter =
        scope.getInstance(GalleryPresenter::class.java)

    private val typeGallery: Int by argument(ARG_TYPE_GALLERY)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.addSystemBottomPadding()
        toolbar.apply {
            addSystemTopPadding()
            toolbar.setNavigationOnClickListener { onBackPressed() }
            title = if (typeGallery == AttachedFilesFragment.PHOTO) {
                getString(R.string.photo_gallery_title)
            } else {
                getString(R.string.video_gallery_title)
            }
        }
        toolbar.apply {
            addSystemTopPadding()
            inflateMenu(R.menu.camera_menu)
            menu.getItem(0).icon = if (typeGallery == AttachedFilesFragment.PHOTO) {
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_photo_camera)
            } else {
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_videocam)
            }
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_camera -> {
                        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        val mediaFile = File(
                            requireActivity().getExternalFilesDir(null)?.absolutePath,
                            System.currentTimeMillis().toString() + ".jpg"
                        )
                        val fileUri = FileProvider.getUriForFile(context, requireActivity().packageName +".fileprovider", mediaFile);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
                        startActivityForResult(intent, 0)
                    }
                }
                true
            }
        }
        btn_attach.setOnClickListener {
            val checkItems = (recycler.adapter as GalleryAdapter).getCheckItems()
            val svm = ViewModelProviders.of(requireActivity()).get(SharedViewModel::class.java)
            svm.setData(checkItems)
            presenter.onBackPressed()
        }
        recycler.layoutManager = GridLayoutManager(requireContext(), 3)
        Dexter.withContext(requireContext())
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    report?.let {
                        if (report.areAllPermissionsGranted()) {
                            when (typeGallery) {
                                AttachedFilesFragment.PHOTO -> {
                                    presenter.getPhotos()
                                }
                                AttachedFilesFragment.VIDEO -> {
                                    presenter.getVideo()
                                }
                            }
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            })
            .withErrorListener {

            }
            .check()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.getPhotos()
    }


    override fun onBackPressed() {
        presenter.onBackPressed()
    }


    override fun setPhotos(it: ArrayList<MediaData>) {
        recycler.adapter = GalleryAdapter(requireContext(), it)
    }

    override fun showMessage(it: Throwable?) {
        Log.d("ASDASD", it.toString())
    }

    companion object {
        private const val ARG_TYPE_GALLERY = "arg_type_gallery"
        fun create(typeGallery: Int) =
            GalleryFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_TYPE_GALLERY, typeGallery)
                }
            }
    }

}