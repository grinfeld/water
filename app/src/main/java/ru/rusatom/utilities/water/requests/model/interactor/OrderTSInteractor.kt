package ru.rusatom.utilities.water.requests.model.interactor

import io.reactivex.Single
import ru.rusatom.utilities.water.requests.entity.OrderTS
import ru.rusatom.utilities.water.requests.model.server.Api
import ru.rusatom.utilities.water.requests.model.system.SchedulersProvider
import javax.inject.Inject

class OrderTSInteractor @Inject constructor(
    private val api: Api,
    private val schedulers: SchedulersProvider
) {
    fun getTS(taskId: Int): Single<List<OrderTS>> {
        return api.getOrderTSByTaskId(taskId)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

}