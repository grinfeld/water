package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.tmc

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_order_tmc.*
import kotlinx.android.synthetic.main.fragment_order_vehicle_content.*
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.entity.OrderTMC
import ru.rusatom.utilities.water.requests.entity.OrderTMC.*
import ru.rusatom.utilities.water.requests.presentation.tmc.OrderTMCPresenter
import ru.rusatom.utilities.water.requests.presentation.tmc.OrderTMCView
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.ui.global.dialog.ConfirmationDelDialog
import ru.rusatom.utilities.water.requests.ui.global.dialog.EditFactCountDialog
import ru.rusatom.utilities.water.requests.ui.tasks.content.pages.adapters.OrderTMCAdapter
import ru.rusatom.utilities.water.requests.utils.addSystemBottomPadding
import ru.rusatom.utilities.water.requests.utils.argument

class OrderTMCFragment : BaseFragment(), OrderTMCView, SwipeRefreshLayout.OnRefreshListener {

    @InjectPresenter
    lateinit var presenter: OrderTMCPresenter

    @ProvidePresenter
    fun providePresenter(): OrderTMCPresenter =
        scope.getInstance(OrderTMCPresenter::class.java)

    override val layoutRes = R.layout.fragment_order_tmc

    private lateinit var orderTMCAdapter: OrderTMCAdapter

    private val taskId: Int by argument(ARG_TASK_ID)


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        view?.addSystemBottomPadding()
        swipeContainer.setOnRefreshListener(this)
        orderTMCAdapter = OrderTMCAdapter(this::openEditFactCount)
        recycler.layoutManager = LinearLayoutManager(requireContext())
        recycler.adapter = orderTMCAdapter
        presenter.getTmc(taskId)
    }


    private fun openEditFactCount(materialOrderItems: MaterialOrderItems) {
        val copy = materialOrderItems.copy()
        val dialog = EditFactCountDialog.create(copy)
        dialog.setTargetFragment(this, 1)
        dialog.show(parentFragmentManager, null)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val materialOrderItems =
            data?.getParcelableExtra<MaterialOrderItems>(EditFactCountDialog.ARG_MATERIAL_ITEM)
        if (materialOrderItems != null) {
            presenter.putFactCont(materialOrderItems)
        }
    }

    companion object {
        private const val ARG_TASK_ID = "arg_task_id"
        fun create(taskId: Int) =
            OrderTMCFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_TASK_ID, taskId)
                }
            }
    }

    override fun showProgress(b: Boolean) {
        showProgressDialog(b)
    }

    override fun showMessage(it: String) {

    }

    override fun orderTMC(it: List<MaterialOrderItems>) {
        orderTMCAdapter.submitList(it)
    }

    override fun showRefreshProgress(b: Boolean) {
        swipeContainer.isRefreshing = b
    }

    override fun onFactContChange() {
        presenter.getTmc(taskId)
    }

    override fun onRefresh() {
        presenter.getTmc(taskId)
    }

}
