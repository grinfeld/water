package ru.rusatom.utilities.water.requests.model.interactor

import ru.rusatom.utilities.water.requests.entity.Work
import ru.rusatom.utilities.water.requests.entity.request.WorkBody
import ru.rusatom.utilities.water.requests.model.server.Api
import ru.rusatom.utilities.water.requests.model.system.SchedulersProvider
import javax.inject.Inject

class WorksInteractor @Inject constructor(
    private val api: Api,
    private val schedulers: SchedulersProvider
) {

    fun getWorkById(taskId: Int) = api.getWorkById(taskId)
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())

    fun delWork(work: Work) = api.deleteWork(work.id)
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())

    fun editWork(work: Work) =
        api.editWork(
            work.id, WorkBody(
                work.id, work.taskId, work.workNameID,
                work.dateEnd, work.descriptor
            )
        )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
}