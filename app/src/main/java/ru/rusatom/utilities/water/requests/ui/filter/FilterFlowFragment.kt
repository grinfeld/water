package ru.rusatom.utilities.water.requests.ui.filter

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.ui.global.FlowFragment

class FilterFlowFragment : FlowFragment(){
    override fun getLaunchScreen() = Screens.Filter
}