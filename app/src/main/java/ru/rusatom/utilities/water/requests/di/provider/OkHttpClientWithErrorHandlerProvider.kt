package ru.rusatom.utilities.water.requests.di.provider

import ru.rusatom.utilities.water.requests.model.server.interceptor.ErrorResponseInterceptor
import okhttp3.OkHttpClient
import javax.inject.Inject
import javax.inject.Provider

class OkHttpClientWithErrorHandlerProvider @Inject constructor(
    private val client: OkHttpClient
) : Provider<OkHttpClient> {

    override fun get() = client
        .newBuilder()
        .addNetworkInterceptor(ErrorResponseInterceptor())
        .build()
}