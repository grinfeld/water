package ru.rusatom.utilities.water.requests.entity

import com.google.gson.annotations.SerializedName

data class UserInfo(
    @SerializedName("isActive")
    var isActive: Boolean,
    @SerializedName("login")
    var login: String,
    @SerializedName("email")
    var email: String,
    @SerializedName("roles")
    var roles: List<Int>,
    @SerializedName("number")
    var number: String,
    @SerializedName("firstName")
    var firstName: String,
    @SerializedName("patronymic")
    var patronymic: String,
    @SerializedName("lastName")
    var lastName: String,
    @SerializedName("positionId")
    var positionId: Int,
    @SerializedName("phone")
    var phone: String,
    @SerializedName("departmentId")
    var departmentId: Int,
    @SerializedName("description")
    var description: String
)