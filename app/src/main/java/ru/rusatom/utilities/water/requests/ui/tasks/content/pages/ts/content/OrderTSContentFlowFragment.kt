package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.ts.content

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.entity.OrderTS.TransportOrderItems
import ru.rusatom.utilities.water.requests.ui.global.FlowFragment

class OrderTSContentFlowFragment(private val transportOrderItems: TransportOrderItems) : FlowFragment() {

    override fun getLaunchScreen() = Screens.OrderTSContent(transportOrderItems)


}