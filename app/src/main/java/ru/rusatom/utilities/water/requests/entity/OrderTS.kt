package ru.rusatom.utilities.water.requests.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class OrderTS(
    @SerializedName("id") val id : Int,
    @SerializedName("taskId") val taskId : Int,
    @SerializedName("taskTypeName") val taskTypeName : String,
    @SerializedName("orderDt") val orderDt : String,
    @SerializedName("orderStateId") val orderStateId : Int,
    @SerializedName("orderStateName") val orderStateName : String,
    @SerializedName("transportOrderItems") val transportOrderItems : List<TransportOrderItems>
){
    @Parcelize
    data class TransportOrderItems (
        @SerializedName("id") val id : Int,
        @SerializedName("vehicleTypeId") val vehicleTypeId : Int,
        @SerializedName("vehicleTypeName") val vehicleTypeName : String,
        @SerializedName("demandStartDt") val demandStartDt : String,
        @SerializedName("demandEndDt") val demandEndDt : String,
        @SerializedName("vehicleId") val vehicleId : String,
        @SerializedName("vehicleNumber") val vehicleNumber : String,
        @SerializedName("driver") val driver : String,
        @SerializedName("transportOrderId") val transportOrderId : Int,
        @SerializedName("transportStateId") val transportStateId : Int,
        @SerializedName("transportStateName") val transportStateName : String,
        @SerializedName("demandAddress") val demandAddress : String,
        @SerializedName("arrivalDt") val arrivalDt : String,
        @SerializedName("departureDt") val departureDt : String
    ) : Parcelable
}