package ru.rusatom.utilities.water.requests.ui.tasks.content.pages.attachedfiles

import ru.rusatom.utilities.water.requests.Screens
import ru.rusatom.utilities.water.requests.ui.global.FlowFragment

class AttachedFilesFlowFragment : FlowFragment() {

    override fun getLaunchScreen() = Screens.AttachedFiles
}