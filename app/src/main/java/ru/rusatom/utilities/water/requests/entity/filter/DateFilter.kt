package ru.rusatom.utilities.water.requests.entity.filter

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DateFilter(val type: Int, val name: String, var isChecked: Boolean) : Parcelable {
    override fun toString(): String {
        return name
    }
}