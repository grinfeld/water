package ru.rusatom.utilities.water.requests.model.server

class ServerError(val errorCode: Int) : RuntimeException()