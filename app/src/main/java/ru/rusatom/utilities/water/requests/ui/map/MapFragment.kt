package ru.rusatom.utilities.water.requests.ui.map

import android.Manifest
import android.os.Bundle
import com.karumi.dexter.Dexter
import com.karumi.dexter.DexterBuilder
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.yandex.mapkit.Animation
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.map.MapObject
import com.yandex.mapkit.map.MapObjectTapListener
import com.yandex.runtime.image.ImageProvider
import kotlinx.android.synthetic.main.fragment_map.*
import ru.rusatom.utilities.water.requests.BuildConfig
import ru.rusatom.utilities.water.requests.R
import ru.rusatom.utilities.water.requests.ui.global.BaseFragment
import ru.rusatom.utilities.water.requests.utils.addSystemBottomPadding
import ru.rusatom.utilities.water.requests.utils.addSystemTopPadding
import ru.rusatom.utilities.water.requests.utils.getBitmapFromVectorDrawable


class MapFragment : BaseFragment(), MapObjectTapListener {
    override val layoutRes = R.layout.fragment_map

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MapKitFactory.setApiKey(BuildConfig.MAPKIT_KEY)
        MapKitFactory.initialize(activity)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        view?.addSystemBottomPadding()
        toolbar.apply {
            setNavigationOnClickListener { }
            addSystemTopPadding()
        }
        val latitude = 53.212776
        val longitude = 50.2457995
        Dexter.withContext(requireContext())
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    report?.let {
                        if (report.areAllPermissionsGranted()) {
                            mapview.map.move(
                                CameraPosition(Point(latitude, longitude), 11.0f, 0.0f, 0.0f),
                                Animation(Animation.Type.SMOOTH, 0F),
                                null
                            )
                            drawMyLocationMark(latitude, longitude)
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    // Remember to invoke this method when the custom rationale is closed
                    // or just by default if you don't want to use any custom rationale.
                    token?.continuePermissionRequest()
                }
            })
            .withErrorListener {

            }
            .check()

        zoom_plus.setOnClickListener {
            mapview.map.move(
                CameraPosition(
                    mapview.map.cameraPosition.target,
                    mapview.map.cameraPosition.zoom + 1, 0.0f, 0.0f
                ),
                Animation(Animation.Type.SMOOTH, 1F),
                null
            )
        }
        zoom_minus.setOnClickListener {
            mapview.map.move(
                CameraPosition(
                    mapview.map.cameraPosition.target,
                    mapview.map.cameraPosition.zoom - 1, 0.0f, 0.0f
                ),
                Animation(Animation.Type.SMOOTH, 1F),
                null
            )
        }
        my_position.setOnClickListener {
            mapview.map.move(
                CameraPosition(
                    Point(latitude, longitude),
                    mapview.map.cameraPosition.zoom,
                    0.0f,
                    0.0f
                ),
                Animation(Animation.Type.SMOOTH, 1F),
                null
            )
        }
    }

    private fun drawMyLocationMark(latitude: Double, longitude: Double) {
        mapview.map.mapObjects.addPlacemark(
            Point(latitude, longitude),
            ImageProvider.fromBitmap(requireContext().getBitmapFromVectorDrawable(R.drawable.ic_point))
        ).addTapListener(this)
    }

    override fun onResume() {
        super.onResume()
        MapKitFactory.getInstance().onStart()
        mapview.onStart()
    }

    override fun onPause() {
        mapview.onStop()
        MapKitFactory.getInstance().onStop()
        super.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapview.onMemoryWarning()
    }

    override fun onMapObjectTap(p0: MapObject, p1: Point): Boolean {

        return true
    }
}