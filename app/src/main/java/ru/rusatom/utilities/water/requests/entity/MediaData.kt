package ru.rusatom.utilities.water.requests.entity

import android.os.Parcelable
import android.provider.MediaStore
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MediaData(
    var id: Int = 0,
    var url: String = "",
    var albumId: Int = 0,
    var isSelected: Boolean = false,
    var mediaType: Int = MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE,
    var duration: Int = 0,
    var dateAdded: String = ""
) : Parcelable