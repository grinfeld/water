package ru.rusatom.utilities.water.requests.presentation.worklist

import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.entity.Work
import ru.rusatom.utilities.water.requests.entity.WorkList
import ru.rusatom.utilities.water.requests.model.interactor.WorksReferenceInteractor
import ru.rusatom.utilities.water.requests.model.system.flow.FlowRouter
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import ru.rusatom.utilities.water.requests.presentation.global.ErrorHandler
import javax.inject.Inject

@InjectViewState
class WorkReferenceListPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val worksReferenceInteractor: WorksReferenceInteractor,
    private val errorHandler: ErrorHandler
) : BasePresenter<WorkReferenceListView>() {


    fun getWorks() {
        worksReferenceInteractor.getWorks()
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { viewState.setWorks(it) },
                { errorHandler.proceed(it, { viewState.showMessage(it) }) }
            )
            .connect()
    }


    fun createListWork(taskId: Int, result: List<WorkList>) {
        if (result.isEmpty()) {
            onBackPressed()
        } else {
            val list = mutableListOf<Work>()
            result.forEach {
                list.add(Work(0, taskId, it.id, it.name, "", ""))
            }
            worksReferenceInteractor.addWorkList(list)
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe(
                    { onBackPressed() },
                    { errorHandler.proceed(it, { viewState.showMessage(it) }) }
                )
                .connect()
        }
    }

    fun onBackPressed() {
        flowRouter.exit()
    }
}