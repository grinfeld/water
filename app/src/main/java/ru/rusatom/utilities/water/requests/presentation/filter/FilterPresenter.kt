package ru.rusatom.utilities.water.requests.presentation.filter

import com.arellomobile.mvp.InjectViewState
import ru.rusatom.utilities.water.requests.model.data.storage.Prefs
import ru.rusatom.utilities.water.requests.model.system.flow.FlowRouter
import ru.rusatom.utilities.water.requests.presentation.global.BasePresenter
import javax.inject.Inject

@InjectViewState
class FilterPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val prefs: Prefs
) : BasePresenter<FilterView>() {
    fun onBackPressed() = flowRouter.exit()

    fun getPrefs() = prefs
}